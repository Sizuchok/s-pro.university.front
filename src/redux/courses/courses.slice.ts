import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { Course } from '../../types/entities/Course'
import { createCourse, getAllCourses, getCourseById, updateCourseById } from './courses.creators'

type CoursesState = {
  list: null | Course[]
  count: number
  editCourse: Course | null
  isError: boolean
  isLoading: boolean
  isSuccess: boolean
  message: string
}

const initialState: CoursesState = {
  list: null,
  count: 0,
  editCourse: null,
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: '',
}

const coursesSlice = createSlice({
  name: 'courses',
  initialState,
  reducers: {
    resetCourses: state => {
      state.isSuccess = false
      state.isLoading = false
      state.isError = false
      state.message = ''
      state.editCourse = null
    },
  },
  extraReducers(builder) {
    // GET ALL COURSES
    builder
      .addCase(getAllCourses.pending, state => {
        state.isLoading = true
      })
      .addCase(getAllCourses.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list = payload.courses
        state.count = payload.count
      })
      .addCase(getAllCourses.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      //GET COURSE BY ID
      .addCase(getCourseById.pending, state => {
        state.isLoading = true
      })
      .addCase(getCourseById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.editCourse = payload
      })
      .addCase(getCourseById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // UPDATE COURSE
      .addCase(updateCourseById.pending, state => {
        state.isLoading = true
      })
      .addCase(updateCourseById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true

        const course = state.list?.find(course => course.id === payload.id)
        if (course) Object.assign(course, payload)
      })
      .addCase(updateCourseById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // CREATE COURSE
      .addCase(createCourse.pending, state => {
        state.isLoading = true
      })
      .addCase(createCourse.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list?.unshift(payload)
      })
      .addCase(createCourse.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
  },
})

export default coursesSlice.reducer
export const { resetCourses } = coursesSlice.actions
