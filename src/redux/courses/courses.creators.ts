import { createAsyncThunk } from '@reduxjs/toolkit'
import { isAxiosError } from 'axios'
import { BaseQueryParams } from '../../types/QueryParams'
import {
  createCourseApi,
  getAllCoursesApi,
  getCourseByIdApi,
  updateCourseByIdApi,
} from '../../api/courses.api'
import { CreateCourse, UpdateCourse } from '../../types/entities/Course'

export const getAllCourses = createAsyncThunk(
  'courses/all',
  async (queryParams: BaseQueryParams, thunkApi) => {
    try {
      const response = await getAllCoursesApi(queryParams)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message =
              'Something went wrong with your request. You can refresh the page and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const getCourseById = createAsyncThunk('courses/get-one', async (id: number, thunkApi) => {
  try {
    const response = await getCourseByIdApi(id)
    return response.data
  } catch (error) {
    if (isAxiosError(error)) {
      const errCode = +error.response?.data.statusCode
      let message

      if (errCode) {
        if (errCode === 400)
          message =
            'Something went wrong with your request. You can refresh the page and try again.'
        if (errCode === 404) message = 'Course not found. You can refresh the page and try again.'
      }

      return thunkApi.rejectWithValue(
        message ?? 'Some error on our side occured. Please, try again later',
      )
    }
  }
})

export const updateCourseById = createAsyncThunk(
  'courses/update-one',
  async ({ id, updateCourseData }: { id: number; updateCourseData: UpdateCourse }, thunkApi) => {
    try {
      const response = await updateCourseByIdApi(id, updateCourseData)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Something went wrong with your request. Check your input data and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const createCourse = createAsyncThunk(
  'courses/create-one',
  async (createCourseData: CreateCourse, thunkApi) => {
    try {
      const response = await createCourseApi(createCourseData)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message =
              'Something went wrong with your request. You can refresh the page and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)
