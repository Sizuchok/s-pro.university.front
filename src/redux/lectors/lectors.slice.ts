import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { Lector } from '../../types/entities/Lector'
import { createLector, getAllLectors, getLectorById, updateLectorById } from './lectors.creators'

type LectorsState = {
  list: null | Lector[]
  count: number
  editLector: Lector | null
  isError: boolean
  isLoading: boolean
  isSuccess: boolean
  message: string
}

const initialState: LectorsState = {
  list: null,
  count: 0,
  editLector: null,
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: '',
}

const lectorsSlice = createSlice({
  name: 'lectors',
  initialState,
  reducers: {
    resetLectors: state => {
      state.isSuccess = false
      state.isLoading = false
      state.isError = false
      state.message = ''
      state.editLector = null
    },
  },
  extraReducers(builder) {
    // GET ALL LECTORS
    builder
      .addCase(getAllLectors.pending, state => {
        state.isLoading = true
      })
      .addCase(getAllLectors.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list = payload.lectors
        state.count = payload.count
      })
      .addCase(getAllLectors.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      //GET LECTOR BY ID
      .addCase(getLectorById.pending, state => {
        state.isLoading = true
      })
      .addCase(getLectorById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.editLector = payload
      })
      .addCase(getLectorById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // UPDATE LECTOR
      .addCase(updateLectorById.pending, state => {
        state.isLoading = true
      })
      .addCase(updateLectorById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true

        const lector = state.list?.find(lector => lector.id === payload.id)
        if (lector) Object.assign(lector, payload)
      })
      .addCase(updateLectorById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // CREATE LECTOR
      .addCase(createLector.pending, state => {
        state.isLoading = true
      })
      .addCase(createLector.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list?.unshift(payload)
      })
      .addCase(createLector.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
  },
})

export default lectorsSlice.reducer
export const { resetLectors } = lectorsSlice.actions
