import { createAsyncThunk } from '@reduxjs/toolkit'
import {
  createLectorApi,
  getAllLectorsApi,
  getLectorByIdApi,
  updateLectorByIdApi,
} from '../../api/lectors.api'
import { isAxiosError } from 'axios'
import { BaseQueryParams } from '../../types/QueryParams'
import { CreateLector, UpdateLector } from '../../types/entities/Lector'

export const getAllLectors = createAsyncThunk(
  'lectors/all',
  async (queryParams: BaseQueryParams, thunkApi) => {
    try {
      const response = await getAllLectorsApi(queryParams)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message =
              'Something went wrong with your request. You can refresh the page and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const getLectorById = createAsyncThunk('lectors/get-one', async (id: number, thunkApi) => {
  try {
    const response = await getLectorByIdApi(id)
    return response.data
  } catch (error) {
    if (isAxiosError(error)) {
      const errCode = +error.response?.data.statusCode
      let message

      if (errCode) {
        if (errCode === 400)
          message =
            'Something went wrong with your request. You can refresh the page and try again.'
        if (errCode === 404) message = 'Lector not found. You can refresh the page and try again.'
      }

      return thunkApi.rejectWithValue(
        message ?? 'Some error on our side occured. Please, try again later',
      )
    }
  }
})

export const updateLectorById = createAsyncThunk(
  'lectors/update-one',
  async ({ id, updateLectorData }: { id: number; updateLectorData: UpdateLector }, thunkApi) => {
    try {
      const response = await updateLectorByIdApi(id, updateLectorData)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Something went wrong with your request. Check your input data and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const createLector = createAsyncThunk(
  'lectors/create-one',
  async (createLectorData: CreateLector, thunkApi) => {
    try {
      const response = await createLectorApi(createLectorData)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Something went wrong with your request. Check your input data and try again.'
          if (errCode === 409) message = 'This email is already associated with an account.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)
