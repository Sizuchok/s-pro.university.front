import { createAsyncThunk } from '@reduxjs/toolkit'
import { isAxiosError } from 'axios'
import { BaseQueryParams } from '../../types/QueryParams'
import {
  addStudentImageApi,
  createStudentApi,
  getAllStudentsApi,
  getStudentByIdApi,
  updateStudentByIdApi,
} from '../../api/students.api'
import { AddStudentImage, CreateStudent, UpdateStudent } from '../../types/entities/Student'

export const getAllStudents = createAsyncThunk(
  'students/all',
  async (queryParams: BaseQueryParams, thunkApi) => {
    try {
      const response = await getAllStudentsApi(queryParams)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message =
              'Something went wrong with your request. You can refresh the page and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const getStudentById = createAsyncThunk('students/get-one', async (id: number, thunkApi) => {
  try {
    const response = await getStudentByIdApi(id)
    return response.data
  } catch (error) {
    if (isAxiosError(error)) {
      const errCode = +error.response?.data.statusCode
      let message

      if (errCode) {
        if (errCode === 400)
          message =
            'Something went wrong with your request. You can refresh the page and try again.'
        if (errCode === 404) message = 'Student not found. You can refresh the page and try again.'
      }

      return thunkApi.rejectWithValue(
        message ?? 'Some error on our side occured. Please, try again later',
      )
    }
  }
})

export const updateStudentById = createAsyncThunk(
  'students/update-one',
  async ({ id, data }: { id: number; data: UpdateStudent }, thunkApi) => {
    try {
      const response = await updateStudentByIdApi(id, data)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Something went wrong with your request. Check your input data and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const addStudentImage = createAsyncThunk(
  'students/add-image',
  async ({ id, data }: { id: number; data: AddStudentImage }, thunkApi) => {
    try {
      const response = await addStudentImageApi(id, data)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Image upload error. Make sure you specified a valid image file.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const createStudent = createAsyncThunk(
  'students/create-one',
  async (data: CreateStudent, thunkApi) => {
    try {
      const response = await createStudentApi(data)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Something went wrong with your request. Check your input data and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)
