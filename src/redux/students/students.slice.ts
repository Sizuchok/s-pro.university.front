import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { Student } from '../../types/entities/Student'
import {
  addStudentImage,
  createStudent,
  getAllStudents,
  getStudentById,
  updateStudentById,
} from './students.creators'

type StudentsState = {
  list: Student[] | null
  count: number
  editStudent: Student | null
  isError: boolean
  isLoading: boolean
  isSuccess: boolean
  message: string
}

const initialState: StudentsState = {
  list: null,
  count: 0,
  editStudent: null,
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: '',
}

const studentsSlice = createSlice({
  name: 'students',
  initialState,
  reducers: {
    resetStudents: state => {
      state.isSuccess = false
      state.isLoading = false
      state.isError = false
      state.message = ''
      state.editStudent = null
    },
  },
  extraReducers(builder) {
    // GET ALL STUDENTS
    builder
      .addCase(getAllStudents.pending, state => {
        state.isLoading = true
      })
      .addCase(getAllStudents.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list = payload.students
        state.count = payload.count
      })
      .addCase(getAllStudents.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      //GET STUDENT BY ID
      .addCase(getStudentById.pending, state => {
        state.isLoading = true
      })
      .addCase(getStudentById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.editStudent = payload
      })
      .addCase(getStudentById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // UPDATE STUDENT
      .addCase(updateStudentById.pending, state => {
        state.isLoading = true
      })
      .addCase(updateStudentById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        const student = state.list?.find(student => student.id === payload.id)
        if (student) Object.assign(student, payload)
      })
      .addCase(updateStudentById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // ADD STUDENT IMAGE
      .addCase(addStudentImage.pending, state => {
        state.isLoading = true
      })
      .addCase(addStudentImage.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true

        const student = state.list?.find(student => student.id === payload.id)
        if (student) Object.assign(student, payload)
      })
      .addCase(addStudentImage.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // CREATE STUDENT
      .addCase(createStudent.pending, state => {
        state.isLoading = true
      })
      .addCase(createStudent.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list?.unshift(payload)
      })
      .addCase(createStudent.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
  },
})

export default studentsSlice.reducer
export const { resetStudents } = studentsSlice.actions
