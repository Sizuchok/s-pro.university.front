import { configureStore } from '@reduxjs/toolkit'
import authSlice from './auth/authSlice'
import lectorsSlice from './lectors/lectors.slice'
import coursesSlice from './courses/courses.slice'
import groupsSlice from './groups/groups.slice'
import studentsSlice from './students/students.slice'

export const store = configureStore({
  reducer: {
    auth: authSlice,
    lectors: lectorsSlice,
    courses: coursesSlice,
    groups: groupsSlice,
    students: studentsSlice,
  },
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch
