import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { Group } from '../../types/entities/Group'
import { createGroup, getAllGroups, getGroupById, updateGroupById } from './groups.creators'

type GroupsState = {
  list: null | Group[]
  count: number
  editGroup: Group | null
  isError: boolean
  isLoading: boolean
  isSuccess: boolean
  message: string
}

const initialState: GroupsState = {
  list: null,
  count: 0,
  editGroup: null,
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: '',
}

const groupsSlice = createSlice({
  name: 'groups',
  initialState,
  reducers: {
    resetGroups: state => {
      state.isSuccess = false
      state.isLoading = false
      state.isError = false
      state.message = ''
      state.editGroup = null
    },
  },
  extraReducers(builder) {
    // GET ALL GROUPS
    builder
      .addCase(getAllGroups.pending, state => {
        state.isLoading = true
      })
      .addCase(getAllGroups.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list = payload.groups
        state.count = payload.count
      })
      .addCase(getAllGroups.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      //GET GROUP BY ID
      .addCase(getGroupById.pending, state => {
        state.isLoading = true
      })
      .addCase(getGroupById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.editGroup = payload
      })
      .addCase(getGroupById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // UPDATE COURSE
      .addCase(updateGroupById.pending, state => {
        state.isLoading = true
      })
      .addCase(updateGroupById.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true

        const group = state.list?.find(group => group.id === payload.id)
        if (group) Object.assign(group, payload)
      })
      .addCase(updateGroupById.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // CREATE GROUP
      .addCase(createGroup.pending, state => {
        state.isLoading = true
      })
      .addCase(createGroup.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.list?.unshift(payload)
      })
      .addCase(createGroup.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
  },
})

export default groupsSlice.reducer
export const { resetGroups } = groupsSlice.actions
