import { createAsyncThunk } from '@reduxjs/toolkit'
import { isAxiosError } from 'axios'
import { BaseQueryParams } from '../../types/QueryParams'
import {
  createGroupApi,
  getAllGroupsApi,
  getGroupByIdApi,
  updateGroupByIdApi,
} from '../../api/groups.api'
import { CreateGroup, UpdateGroup } from '../../types/entities/Group'

export const getAllGroups = createAsyncThunk(
  'groups/all',
  async (queryParams: BaseQueryParams, thunkApi) => {
    try {
      const response = await getAllGroupsApi(queryParams)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Something went wrong with your request. Check your input data and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const getGroupById = createAsyncThunk('groups/get-one', async (id: number, thunkApi) => {
  try {
    const response = await getGroupByIdApi(id)
    return response.data
  } catch (error) {
    if (isAxiosError(error)) {
      const errCode = +error.response?.data.statusCode
      let message

      if (errCode) {
        if (errCode === 400)
          message =
            'Something went wrong with your request. You can refresh the page and try again.'
        if (errCode === 404) message = 'Group not found. You can refresh the page and try again.'
      }

      return thunkApi.rejectWithValue(
        message ?? 'Some error on our side occured. Please, try again later',
      )
    }
  }
})

export const updateGroupById = createAsyncThunk(
  'groups/update-one',
  async ({ id, data }: { id: number; data: UpdateGroup }, thunkApi) => {
    try {
      const response = await updateGroupByIdApi(id, data)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Something went wrong with your request. Check your input data and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const createGroup = createAsyncThunk(
  'groups/create-one',
  async (data: CreateGroup, thunkApi) => {
    try {
      const response = await createGroupApi(data)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message =
              'Something went wrong with your request. You can refresh the page and try again.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)
