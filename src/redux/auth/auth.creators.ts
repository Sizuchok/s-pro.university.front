import { createAsyncThunk } from '@reduxjs/toolkit'
import {
  forgotPasswordApi,
  getMeApi,
  logOutApi,
  resetPasswordApi,
  signInApi,
  signUpApi,
} from '../../api/auth.api'
import {
  ForgotPasswordFormData,
  ResetPasswordData,
  UserCredentials,
  UserSignUpData,
} from '../../types/entities/User'
import { isAxiosError } from 'axios'

export const signIn = createAsyncThunk(
  'auth/sign-in',
  async (credentials: UserCredentials, thunkApi) => {
    try {
      const response = await signInApi(credentials)

      localStorage.setItem('accessToken', response.data.accessToken)

      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400 || errCode === 401) message = 'Invalid email or password'
          if (errCode === 500) message = ''
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const signUp = createAsyncThunk(
  'auth/sign-up',
  async (userData: UserSignUpData, thunkApi) => {
    try {
      const response = await signUpApi(userData)
      localStorage.setItem('accessToken', response.data.accessToken)
      return response.data
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Invalid form data. Please check that the data you entered is correct.'
          else if (errCode === 409) message = 'This email is already associated with an account.'
        }
        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later.',
        )
      }
      throw error
    }
  },
)

export const getMe = createAsyncThunk('auth/get-me', async (arg, thunkApi) => {
  try {
    const response = await getMeApi()
    return response.data
  } catch (error) {
    if (isAxiosError(error)) {
      return thunkApi.rejectWithValue('Your session has expired. Please re-login.')
    }
    throw error
  }
})

export const logOut = createAsyncThunk('auth/log-out', async () => {
  await logOutApi()
})

export const forgotPassword = createAsyncThunk(
  'auth/reset-password-request',
  async (forgotPasswordFormData: ForgotPasswordFormData, thunkApi) => {
    try {
      await forgotPasswordApi(forgotPasswordFormData)
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message = 'Invalid email. Please, make sure you specified your email correctly.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)

export const resetPassword = createAsyncThunk(
  'auth/reset-password',
  async (resetPasswordData: ResetPasswordData, thunkApi) => {
    try {
      await resetPasswordApi(resetPasswordData)
    } catch (error) {
      if (isAxiosError(error)) {
        const errCode = +error.response?.data.statusCode
        let message

        if (errCode) {
          if (errCode === 400)
            message =
              'Something is wrong with your request. Make sure you use reset link from your email.'
        }

        return thunkApi.rejectWithValue(
          message ?? 'Some error on our side occured. Please, try again later',
        )
      }
    }
  },
)
