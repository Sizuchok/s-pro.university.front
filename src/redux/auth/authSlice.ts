import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { forgotPassword, getMe, logOut, resetPassword, signIn, signUp } from './auth.creators'
import { User } from '../../types/entities/User'

type AuthState = {
  user: null | User
  isError: boolean
  isLoading: boolean
  isSuccess: boolean
  message: string
}

const initialState: AuthState = {
  user: null,
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: '',
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    reset: state => {
      state.isSuccess = false
      state.isLoading = false
      state.isError = false
      state.message = ''
    },
  },
  extraReducers(builder) {
    builder
      .addCase(signIn.pending, state => {
        state.isLoading = true
      })
      .addCase(signIn.fulfilled, state => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(signIn.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      //SIGN UP
      .addCase(signUp.pending, state => {
        state.isLoading = true
      })
      .addCase(signUp.fulfilled, state => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(signUp.rejected, (state, { payload }) => {
        state.isLoading = false
        state.isError = true
        state.message = payload as string
      })
      // GET ME
      .addCase(getMe.pending, state => {
        state.isLoading = true
      })
      .addCase(getMe.fulfilled, (state, { payload }) => {
        state.isLoading = false
        state.isSuccess = true
        state.user = payload
      })
      .addCase(getMe.rejected, (state, { payload }) => {
        state.isError = true
        state.message = payload as string
      })
      // LOG OUT
      .addCase(logOut.fulfilled, state => {
        state.user = null
      })
      //FORGOT PASSWORD
      .addCase(forgotPassword.pending, state => {
        state.isLoading = true
      })
      .addCase(forgotPassword.fulfilled, state => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(forgotPassword.rejected, (state, { payload }) => {
        state.isError = true
        state.message = payload as string
      })
      //RESET PASSWORD
      .addCase(resetPassword.pending, state => {
        state.isLoading = true
      })
      .addCase(resetPassword.fulfilled, state => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(resetPassword.rejected, (state, { payload }) => {
        state.isError = true
        state.message = payload as string
      })
  },
})

export default authSlice.reducer
export const { reset } = authSlice.actions
