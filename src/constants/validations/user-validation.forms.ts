import { z } from 'zod'

export const signInSchema = z.object({
  email: z.string().email("Sorry, but that email doesn't look right."),
  password: z.string().min(8, 'Password must be at least 8 characters long.'),
})

export const signUpSchema = z
  .object({
    name: z
      .string()
      .min(2, 'Name must be at least 2 characters long.')
      .max(16, 'Name must be at most 16 characters long.'),
    surname: z
      .string()
      .min(2, 'Surname must be at least 2 characters long.')
      .max(25, 'Surname must be at most 25 characters long.')
      .optional(),
    email: z.string().email("Sorry, but that email doesn't look right."),
    password: z
      .string()
      .min(8, 'Password must be at least 8 characters long.')
      .max(24, 'Password must be at most 24 characters long.')
      .refine(value => /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/.test(value), {
        message: 'Password must have a digit, a lowercase, and an uppercase letter.',
      }),
    confirmPassword: z.string(),
  })
  .refine(data => data.password === data.confirmPassword, {
    message: 'Passwords must match.',
    path: ['confirmPassword'],
  })

export const forgotPasswordSchema = signInSchema.pick({ email: true })

export const resetPasswordSchema = z
  .object({
    password: z
      .string()
      .min(8, 'Password must be at least 8 characters long.')
      .max(24, 'Password must be at most 24 characters long.')
      .refine(value => /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/.test(value), {
        message: 'Password must have a digit, a lowercase, and an uppercase letter.',
      }),
    confirmPassword: z.string(),
  })
  .refine(data => data.password === data.confirmPassword, {
    message: 'Passwords must match.',
    path: ['confirmPassword'],
  })
