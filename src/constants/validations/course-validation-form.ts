import { z } from 'zod'

export const createCourseSchema = z.object({
  name: z
    .string()
    .min(3, 'Name must be at least 3 characters long.')
    .max(100, 'Name must be at most 100 characters long.'),
  description: z.string().max(200, 'Description must be at most 200 characters long.'),
  hours: z.coerce.number().int().positive().max(100, 'Max hours value is 100.'),
})

export const editCourseSchema = createCourseSchema.partial()
