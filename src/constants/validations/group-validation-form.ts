import { z } from 'zod'

export const createGroupSchema = z.object({
  name: z
    .string()
    .min(2, 'Name must be at least 2 characters long.')
    .max(10, 'Name must be at most 10 characters long.'),
})

export const editGroupSchema = createGroupSchema.partial()
