import { z } from 'zod'

export const createLectorSchema = z.object({
  name: z
    .string()
    .min(2, 'Name must be at least 2 characters long.')
    .max(16, 'Name must be at most 16 characters long.'),
  surname: z
    .string()
    .min(2, 'Surname must be at least 2 characters long.')
    .max(25, 'Surname must be at most 25 characters long.')
    .nullable()
    .optional()
    .or(z.literal('').transform(value => (value === '' ? null : value))),
  email: z.string().email("Sorry, but that email doesn't look right."),
  password: z
    .string()
    .min(8, 'Password must be at least 8 characters long.')
    .max(24, 'Password must be at most 24 characters long.')
    .refine(value => /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/.test(value), {
      message: 'Password must have a digit, a lowercase, and an uppercase letter.',
    }),
})

export const editLectorSchema = createLectorSchema.extend({
  password: z
    .string()
    .min(8, 'Password must be at least 8 characters long.')
    .max(24, 'Password must be at most 24 characters long.')
    .refine(value => /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/.test(value), {
      message: 'Password must have a digit, a lowercase, and an uppercase letter.',
    })
    .or(z.literal('').transform(value => (value === '' ? undefined : value))),
})
