import { z } from 'zod'

export const createStudentSchema = z.object({
  name: z
    .string()
    .min(2, 'Name must be at least 2 characters long.')
    .max(16, 'Name must be at most 16 characters long.'),
  surname: z
    .string()
    .min(2, 'Surname must be at least 2 characters long.')
    .max(25, 'Surname must be at most 25 characters long.'),
  email: z.string().email("Sorry, but that email doesn't look right."),
  age: z.coerce
    .number()
    .positive({ message: 'Age must be a positive number.' })
    .int({ message: 'Age must be an integer.' })
    .min(14, 'Age must be at least 14.')
    .max(90, 'Age must be at most 90.'),
})

export const updateStudentSchema = createStudentSchema.partial().extend({
  groupId: z.coerce.number().positive().int().optional(),
  file: z.any().optional(),
  courseIds: z.array(z.number()).optional(),
})
