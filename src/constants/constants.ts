export const ACCESS_TOKEN_KEY = 'accessToken'
export const RESET_PASSWORD_URL_TOKEN_KEY = 'resetToken'
export const RESET_PASSWORD_URL_ID_KEY = 'id'

export const API_SERVER_URL = 'https://university-back.onrender.com/api/v1'
// export const API_SERVER_URL = 'http://localhost:3090/api/v1'
