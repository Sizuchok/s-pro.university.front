export const baseSortOptions = [
  { value: 'name-asc', label: 'A-Z' },
  { value: 'name-desc', label: 'Z-A' },
  { value: 'createdAt-desc', label: 'Newest first' },
  { value: 'createdAt-asc', label: 'Oldest first' },
  { value: 'updatedAt-desc', label: 'Recently updated' },
  { value: 'updatedAt-asc', label: 'Least recently updated' },
  { value: '', label: 'All' },
]
