import { StylesConfig } from 'react-select'
import { StringStringOption } from '../../types/react-select'

export const reactSelectDashboardStyles: StylesConfig<StringStringOption, false> = {
  menu: (base, props) => ({
    ...base,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    marginTop: '-4px',
    boxShadow: 'none',
    border: '1px solid #E4E7ED',
    borderTop: 'none',
  }),
  indicatorSeparator: (base, props) => ({
    ...base,
    display: 'none',
  }),
  dropdownIndicator: (base, props) => ({
    ...base,
    color: '#3B4360',
    transition: 'all .2s ease',
    transform: props.selectProps.menuIsOpen ? 'rotate(180deg)' : undefined,
  }),
  option: (base, state) => ({
    ...base,
    fontSize: '14px',
    fontWeight: 500,
    color: state.isFocused || state.isSelected ? '#3B4360' : '#9f9fb6',
    backgroundColor: 'transparent',
    paddingLeft: '12px',
    cursor: 'pointer',
    ':active': {
      backgroundColor: 'transparent',
    },
  }),
  valueContainer: (base, state) => ({
    ...base,
    paddingLeft: 0,
  }),
  singleValue: (base, props) => ({
    ...base,
    marginLeft: 0,
  }),
  control: (base, state) => ({
    ...base,
    fontFamily: 'Nunito',
    fontSize: '14px',
    fontWeight: 500,
    width: '140px',
    borderRadius: '6px',
    cursor: 'pointer',
    borderColor: state.isFocused ? 'transparent' : base.borderColor,
    borderBottomLeftRadius: state.menuIsOpen ? 0 : '6px',
    borderBottomRightRadius: state.menuIsOpen ? 0 : '6px',
    boxShadow: 'none',
    paddingLeft: '12px',

    ':hover': {
      border: '1px solid #e4e7ed',
    },
    border: '1px solid #e4e7ed',
  }),
}
