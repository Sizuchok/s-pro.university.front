import { StylesConfig } from 'react-select'

export const reactSelectPrimaryStyles: StylesConfig<any, any> = {
  option: (base, state) => ({
    ...base,
    color: state.isFocused || state.isSelected ? '#000' : '#9f9fb6',
    backgroundColor: 'transparent',
    cursor: 'pointer',
    ':active': {
      backgroundColor: 'transparent',
    },
  }),
}
