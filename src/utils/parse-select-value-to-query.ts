export const parseSelectValueToQuery = (
  value: string | undefined,
): { sortBy: string; order: string } => {
  if (!value) return { sortBy: '', order: '' }

  const [sortBy, order] = value?.split('-') ?? []

  return { sortBy, order }
}
