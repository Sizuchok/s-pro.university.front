import axios, {
  AxiosRequestConfig,
  AxiosResponse,
  RawAxiosRequestHeaders,
  isAxiosError,
} from 'axios'
import { ACCESS_TOKEN_KEY, API_SERVER_URL } from '../constants/constants'

type ExpandedAxiosRequestConfig = AxiosRequestConfig & {
  path?: string
  token?: string
  onError?: any
  isFileUpload?: boolean
}

const getToken = () => {
  return localStorage.getItem(ACCESS_TOKEN_KEY)
}

export const apiCaller = async (opts: ExpandedAxiosRequestConfig = {}): Promise<AxiosResponse> => {
  const { url = null, method = 'get', params = {}, data = null, isFileUpload = false } = opts
  const headers: RawAxiosRequestHeaders = {}

  if (!isFileUpload) headers['Content-Type'] = 'application/json'

  const token = getToken()

  if (token) {
    headers['Authorization'] = `Bearer ${token}`
  }

  const requestURL = `${API_SERVER_URL}/${url}`

  const config = {
    url: requestURL,
    method,
    data,
    headers,
    params,
  }

  const axiosApiInstance = axios.create(config)

  axiosApiInstance.interceptors.response.use(
    response => response,
    error => {
      const errorCode = error.response?.data.statusCode
      if (isAxiosError(error)) {
        if (error.code === 'ERR_NETWORK' || errorCode === 401) {
          localStorage.removeItem(ACCESS_TOKEN_KEY)
        }
      }

      return Promise.reject(error)
    },
  )
  // DELETE FOR PROD
  await new Promise(resolve => setTimeout(resolve, 2000))

  return axiosApiInstance.request(config)
}
