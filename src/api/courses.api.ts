import { AxiosResponse } from 'axios'
import { apiCaller } from '../utils/api-caller'
import { BaseQueryParams } from '../types/QueryParams'
import { CreateCourse, UpdateCourse } from '../types/entities/Course'

export const getAllCoursesApi = async (queryParams: BaseQueryParams): Promise<AxiosResponse> => {
  const queryParamsObj = new URLSearchParams(queryParams)

  return apiCaller({
    method: 'GET',
    url: `courses?${queryParamsObj.toString()}`,
  })
}

export const getCourseByIdApi = async (id: number): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'GET',
    url: `courses/${id}`,
  })
}

export const updateCourseByIdApi = async (
  id: number,
  data: UpdateCourse,
): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'PATCH',
    url: `courses/${id}`,
    data,
  })
}

export const createCourseApi = async (data: CreateCourse): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'POST',
    url: `courses`,
    data,
  })
}
