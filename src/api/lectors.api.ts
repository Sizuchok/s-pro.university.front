import { AxiosResponse } from 'axios'
import { apiCaller } from '../utils/api-caller'
import { BaseQueryParams } from '../types/QueryParams'
import { CreateLector, UpdateLector } from '../types/entities/Lector'

export const getAllLectorsApi = async (queryParams: BaseQueryParams): Promise<AxiosResponse> => {
  const queryParamsObj = new URLSearchParams(queryParams)

  return apiCaller({
    method: 'GET',
    url: `lectors?${queryParamsObj.toString()}`,
  })
}

export const getLectorByIdApi = async (id: number): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'GET',
    url: `lectors/${id}`,
  })
}

export const updateLectorByIdApi = async (
  id: number,
  data: UpdateLector,
): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'PATCH',
    url: `lectors/${id}`,
    data,
  })
}

export const createLectorApi = async (data: CreateLector): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'POST',
    url: `lectors`,
    data,
  })
}
