import { AxiosResponse } from 'axios'
import { apiCaller } from '../utils/api-caller'
import {
  ForgotPasswordFormData,
  ResetPasswordData,
  UserCredentials,
  UserSignUpData,
} from '../types/entities/User'
import { ACCESS_TOKEN_KEY } from '../constants/constants'

export const signInApi = async (credentials: UserCredentials): Promise<AxiosResponse> =>
  apiCaller({
    method: 'POST',
    url: 'auth/sign-in',
    data: credentials,
  })

export const signUpApi = async (userData: UserSignUpData): Promise<AxiosResponse> =>
  apiCaller({
    method: 'POST',
    url: 'auth/sign-up',
    data: userData,
  })

export const getMeApi = async (): Promise<AxiosResponse> =>
  apiCaller({
    method: 'GET',
    url: 'lectors/me',
  })

export const logOutApi = async () => {
  localStorage.removeItem(ACCESS_TOKEN_KEY)
}

export const forgotPasswordApi = async (
  forgotPasswordFormData: ForgotPasswordFormData,
): Promise<AxiosResponse> =>
  apiCaller({
    method: 'POST',
    url: 'auth/reset-password-request',
    data: forgotPasswordFormData,
  })

export const resetPasswordApi = async (
  resetPasswordData: ResetPasswordData,
): Promise<AxiosResponse> =>
  apiCaller({
    method: 'POST',
    url: 'auth/reset-password',
    data: resetPasswordData,
  })
