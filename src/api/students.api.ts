import { AxiosResponse } from 'axios'
import { apiCaller } from '../utils/api-caller'
import { BaseQueryParams } from '../types/QueryParams'
import { AddStudentImage, CreateStudent, UpdateStudent } from '../types/entities/Student'

export const getAllStudentsApi = async (queryParams: BaseQueryParams): Promise<AxiosResponse> => {
  const queryParamsObj = new URLSearchParams(queryParams)

  return apiCaller({
    method: 'GET',
    url: `students?${queryParamsObj.toString()}`,
  })
}

export const getStudentByIdApi = async (id: number): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'GET',
    url: `students/${id}`,
  })
}

export const updateStudentByIdApi = async (
  id: number,
  data: UpdateStudent,
): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'PATCH',
    url: `students/${id}`,
    data,
  })
}

export const addStudentImageApi = async (
  id: number,
  data: AddStudentImage,
): Promise<AxiosResponse> => {
  const formData = new FormData()

  formData.append('file', data.file)

  return apiCaller({
    method: 'PATCH',
    url: `students/${id}/image`,
    data: formData,
    isFileUpload: true,
  })
}

export const createStudentApi = async (data: CreateStudent): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'POST',
    url: `students`,
    data,
  })
}
