import { AxiosResponse } from 'axios'
import { apiCaller } from '../utils/api-caller'
import { BaseQueryParams } from '../types/QueryParams'
import { UpdateGroup } from '../types/entities/Group'

export const getAllGroupsApi = async (queryParams: BaseQueryParams): Promise<AxiosResponse> => {
  const queryParamsObj = new URLSearchParams(queryParams)

  return apiCaller({
    method: 'GET',
    url: `groups?${queryParamsObj.toString()}`,
  })
}

export const getGroupByIdApi = async (id: number): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'GET',
    url: `groups/${id}`,
  })
}

export const updateGroupByIdApi = async (id: number, data: UpdateGroup): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'PATCH',
    url: `groups/${id}`,
    data,
  })
}

export const createGroupApi = async (data: UpdateGroup): Promise<AxiosResponse> => {
  return apiCaller({
    method: 'POST',
    url: `groups`,
    data,
  })
}
