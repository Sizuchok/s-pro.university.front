import { z } from 'zod'
import {
  createLectorSchema,
  editLectorSchema,
} from '../../constants/validations/lector-validation.form'
import { CoreEntity } from './CoreEntity'
import { Prettify } from '../../utils/utility-types'

export type Lector = Prettify<CoreEntity> & {
  name: string
  surname: string
  email: string
  password: string
}

export type CreateLector = z.infer<typeof createLectorSchema>

export type UpdateLector = z.infer<typeof editLectorSchema>
