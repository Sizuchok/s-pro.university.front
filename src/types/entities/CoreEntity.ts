export type CoreEntity = {
  id: number
  createdAt: Date
  updatedAt: Date
}
