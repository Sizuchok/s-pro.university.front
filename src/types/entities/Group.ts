import { z } from 'zod'
import {
  createGroupSchema,
  editGroupSchema,
} from '../../constants/validations/group-validation-form'
import { CoreEntity } from './CoreEntity'
import { Prettify } from '../../utils/utility-types'

export type Group = Prettify<CoreEntity> & CreateGroup

export type CreateGroup = z.infer<typeof createGroupSchema>
export type UpdateGroup = z.infer<typeof editGroupSchema>
