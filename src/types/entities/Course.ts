import { z } from 'zod'
import {
  createCourseSchema,
  editCourseSchema,
} from '../../constants/validations/course-validation-form'
import { CoreEntity } from './CoreEntity'
import { Prettify } from '../../utils/utility-types'

export type Course = Prettify<CoreEntity> &
  CreateCourse & {
    studentsCount?: number[]
  }

export type CreateCourse = z.infer<typeof createCourseSchema>
export type UpdateCourse = z.infer<typeof editCourseSchema>
