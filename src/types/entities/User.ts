import { z } from 'zod'
import {
  forgotPasswordSchema,
  resetPasswordSchema,
  signInSchema,
  signUpSchema,
} from '../../constants/validations/user-validation.forms'
import { CoreEntity } from './CoreEntity'
import { Prettify } from '../../utils/utility-types'

export type User = Prettify<CoreEntity> & {
  name: string
  surname: string
  email: string
}

export type UserCredentials = z.infer<typeof signInSchema>

export type UserData = UserCredentials & {
  name: string
}

export type UserSignUpFormData = z.infer<typeof signUpSchema>

export type UserSignUpData = Omit<UserSignUpFormData, 'confirmPassword'>

export type ForgotPasswordFormData = z.infer<typeof forgotPasswordSchema>

export type ResetPasswordFormData = z.infer<typeof resetPasswordSchema>

export type ResetPasswordData = {
  lectorId: number
  token: string
  newPassword: string
}
