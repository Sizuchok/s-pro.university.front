import { z } from 'zod'
import {
  createStudentSchema,
  updateStudentSchema,
} from '../../constants/validations/student-validation.form'
import { CoreEntity } from './CoreEntity'
import { Course } from './Course'
import { Prettify } from '../../utils/utility-types'

export type CreateStudent = z.infer<typeof createStudentSchema>
export type UpdateStudentForm = z.infer<typeof updateStudentSchema>
export type UpdateStudent = Omit<UpdateStudentForm, 'file'>

export type Student = Prettify<CoreEntity> &
  CreateStudent & {
    groupId: number | null
    imagePath: string | null
    groupName: string | null
    courses: Course[]
  }

export type AddStudentImage = {
  file: File
}
