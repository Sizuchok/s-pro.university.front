export type NumberStringOption = {
  value: number
  label: string
}

export type StringStringOption = {
  value: string
  label: string
}
