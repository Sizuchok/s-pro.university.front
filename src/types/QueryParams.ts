export type BaseQueryParams = {
  query?: string
  sortBy?: string
  order?: string
  limit?: string
  offset?: string
}
