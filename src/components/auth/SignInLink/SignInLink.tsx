import { Link } from 'react-router-dom'
import styles from './sign-in-link.module.scss'

const SignInLink = () => {
  return (
    <div className={styles['sign-in-link']}>
      Already have an account?&nbsp;
      <Link className={styles.link} to="/sign-in">
        Sign in instead
      </Link>
    </div>
  )
}

export default SignInLink
