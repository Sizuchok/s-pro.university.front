import { Link } from 'react-router-dom'
import styles from './reset-password-link.module.scss'

const ResetPasswordLink = () => {
  return (
    <div className={styles['reset-password-link']}>
      <Link className={styles.resetLink} to="/forgot-password">
        Forgot your password?
      </Link>
    </div>
  )
}

export default ResetPasswordLink
