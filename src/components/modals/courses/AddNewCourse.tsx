import { useNavigate } from 'react-router-dom'
import FormTitle from '../../common/FormTitle/FormTitle'
import EntityModalLayout from '../../layouts/modal/EntityModalLayout/EntityModalLayout'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { useEffect } from 'react'
import { CreateCourse } from '../../../types/entities/Course'
import { createCourse } from '../../../redux/courses/courses.creators'
import AddNewCoureForm from '../../Forms/courses/AddNewCourseForm'

const AddNewCourse = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess } = useAppSelector(state => state.courses)
  const returnTo = '/courses'

  const onSubmit = (data: CreateCourse) => {
    dispatch(createCourse(data))
  }

  useEffect(() => {
    if (isSuccess) navigate(returnTo)
  }, [dispatch, navigate, isSuccess])

  return (
    <EntityModalLayout returnTo={returnTo}>
      <FormTitle title={`Add new course`} noLogo />
      <AddNewCoureForm isLoading={isLoading} onSubmit={onSubmit} />
    </EntityModalLayout>
  )
}

export default AddNewCourse
