import { useNavigate, useParams } from 'react-router-dom'
import FormTitle from '../../common/FormTitle/FormTitle'
import EntityModalLayout from '../../layouts/modal/EntityModalLayout/EntityModalLayout'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { useEffect } from 'react'
import { UpdateCourse } from '../../../types/entities/Course'
import { getCourseById, updateCourseById } from '../../../redux/courses/courses.creators'
import EditCourseForm from '../../Forms/courses/EditCourseForm'

const EditCourse = () => {
  const returnTo = '/courses'

  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess, editCourse } = useAppSelector(state => state.courses)
  const params = useParams()
  const id = Number(params.id)

  if (!id) navigate(returnTo)

  const onSubmit = (data: UpdateCourse) => {
    dispatch(
      updateCourseById({
        id,
        updateCourseData: data,
      }),
    )
  }

  useEffect(() => {
    dispatch(getCourseById(id))
  }, [])

  useEffect(() => {
    if (isSuccess) navigate(returnTo)
  }, [dispatch, navigate, isSuccess])

  return (
    <EntityModalLayout returnTo={returnTo}>
      <FormTitle title={`Edit ${editCourse?.name ?? 'course'}`} noLogo />
      <EditCourseForm course={editCourse} onSubmit={onSubmit} isLoading={isLoading} />
    </EntityModalLayout>
  )
}

export default EditCourse
