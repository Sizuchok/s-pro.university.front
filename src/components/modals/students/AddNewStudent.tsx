import { useNavigate } from 'react-router-dom'
import FormTitle from '../../common/FormTitle/FormTitle'
import EntityModalLayout from '../../layouts/modal/EntityModalLayout/EntityModalLayout'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { useEffect } from 'react'
import { CreateStudent } from '../../../types/entities/Student'
import { createStudent } from '../../../redux/students/students.creators'
import AddNewStudentForm from '../../Forms/students/AddNewStudentForm'

const AddNewStudent = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess } = useAppSelector(state => state.students)

  const onSubmit = (data: CreateStudent) => {
    dispatch(createStudent(data))
  }

  useEffect(() => {
    if (isSuccess) navigate('/students')
  }, [dispatch, navigate, isSuccess])

  return (
    <EntityModalLayout returnTo="/students">
      <FormTitle title={`Add new student`} noLogo />
      <AddNewStudentForm isLoading={isLoading} onSubmit={onSubmit} />
    </EntityModalLayout>
  )
}

export default AddNewStudent
