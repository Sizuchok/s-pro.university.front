import { useNavigate } from 'react-router-dom'
import FormTitle from '../../common/FormTitle/FormTitle'
import EntityModalLayout from '../../layouts/modal/EntityModalLayout/EntityModalLayout'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { useEffect } from 'react'
import { createLector } from '../../../redux/lectors/lectors.creators'
import { CreateLector } from '../../../types/entities/Lector'
import AddNewLectorForm from '../../Forms/lectors/AddNewLectorForm'

const AddNewLector = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess, isError, message } = useAppSelector(state => state.lectors)

  const onSubmit = (data: CreateLector) => {
    dispatch(createLector(data))
  }

  useEffect(() => {
    if (isSuccess) navigate('/lectors')
  }, [dispatch, navigate, isSuccess])

  return (
    <EntityModalLayout returnTo="/lectors">
      <FormTitle title={`Add new lector`} noLogo />
      <AddNewLectorForm isLoading={isLoading} onSubmit={onSubmit} />
    </EntityModalLayout>
  )
}

export default AddNewLector
