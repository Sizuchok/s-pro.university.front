import { useLocation, useNavigate, useParams } from 'react-router-dom'
import FormTitle from '../../common/FormTitle/FormTitle'
import AuthPrimaryLayout from '../../layouts/auth/AuthPrimaryLayout'
import EntityModalLayout from '../../layouts/modal/EntityModalLayout/EntityModalLayout'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { useEffect } from 'react'
import { getLectorById, updateLectorById } from '../../../redux/lectors/lectors.creators'
import EditLectorForm from '../../Forms/lectors/EditLectorForm'
import { UpdateLector } from '../../../types/entities/Lector'

const EditLector = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess, editLector } = useAppSelector(state => state.lectors)
  const params = useParams()
  const id = Number(params.id)

  if (!id) navigate('/lectors')

  const onSubmit = (data: UpdateLector) => {
    dispatch(
      updateLectorById({
        id,
        updateLectorData: data,
      }),
    )
  }

  useEffect(() => {
    dispatch(getLectorById(id))
  }, [])

  useEffect(() => {
    if (isSuccess) navigate('/lectors')
  }, [dispatch, navigate, isSuccess])

  return (
    <EntityModalLayout returnTo="/lectors">
      <FormTitle title={`Edit ${editLector?.name ?? 'lector'}`} noLogo />
      <EditLectorForm lector={editLector} onSubmit={onSubmit} isLoading={isLoading} />
    </EntityModalLayout>
  )
}

export default EditLector
