import { useNavigate } from 'react-router-dom'
import FormTitle from '../../common/FormTitle/FormTitle'
import EntityModalLayout from '../../layouts/modal/EntityModalLayout/EntityModalLayout'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { useEffect } from 'react'
import { CreateGroup } from '../../../types/entities/Group'
import { createGroup } from '../../../redux/groups/groups.creators'
import AddNewGroupForm from '../../Forms/groups/AddNewGroupForm'

const AddNewGroup = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess } = useAppSelector(state => state.groups)
  const returnTo = '/groups'

  const onSubmit = (data: CreateGroup) => {
    dispatch(createGroup(data))
  }

  useEffect(() => {
    if (isSuccess) navigate(returnTo)
  }, [dispatch, navigate, isSuccess])

  return (
    <EntityModalLayout returnTo={returnTo}>
      <FormTitle title={`Add new group`} noLogo />
      <AddNewGroupForm isLoading={isLoading} onSubmit={onSubmit} />
    </EntityModalLayout>
  )
}

export default AddNewGroup
