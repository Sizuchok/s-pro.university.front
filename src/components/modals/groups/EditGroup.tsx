import { useNavigate, useParams } from 'react-router-dom'
import FormTitle from '../../common/FormTitle/FormTitle'
import EntityModalLayout from '../../layouts/modal/EntityModalLayout/EntityModalLayout'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { useEffect } from 'react'
import { getGroupById, updateGroupById } from '../../../redux/groups/groups.creators'
import { UpdateGroup } from '../../../types/entities/Group'
import EditGroupForm from '../../Forms/groups/EditGroupForm'

const EditGroup = () => {
  const returnTo = '/groups'

  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess, editGroup } = useAppSelector(state => state.groups)
  const params = useParams()
  const id = Number(params.id)

  if (!id) navigate(returnTo)

  const onSubmit = (data: UpdateGroup) => {
    dispatch(updateGroupById({ id, data }))
  }

  useEffect(() => {
    dispatch(getGroupById(id))
  }, [])

  useEffect(() => {
    if (isSuccess) navigate(returnTo)
  }, [dispatch, navigate, isSuccess])

  return (
    <EntityModalLayout returnTo={returnTo}>
      <FormTitle title={`Edit ${editGroup?.name ?? 'group'}`} noLogo />
      <EditGroupForm group={editGroup} onSubmit={onSubmit} isLoading={isLoading} />
    </EntityModalLayout>
  )
}

export default EditGroup
