import { ReactNode } from 'react'
import DashboardHeader from '../../dashboard/DashboardHeader/DashboardHeader'
import DashboardSidebar from '../../dashboard/DashboardSidebar/DashboardSidebar'
import styles from './dashboard-primary-layout.module.scss'

type DashboardPrimaryLayoutProps = {
  tabName: string
  children: ReactNode
}

const DashboardPrimaryLayout = ({ tabName, children }: DashboardPrimaryLayoutProps) => {
  return (
    <div className={styles.dashboard}>
      <DashboardSidebar />
      <main>
        <DashboardHeader tabName={tabName} />
        <div className={styles['entity-view-contaiter']}>{children}</div>
      </main>
    </div>
  )
}

export default DashboardPrimaryLayout
