import DashboardHeader from '../../dashboard/DashboardHeader/DashboardHeader'
import styles from './student-details-layout.module.scss'
import logoPurple from '../../../assets/svg/logo_details.svg'
import { ReactNode } from 'react'

type StudentDetailsLayoutProps = {
  children: ReactNode
}

const StudentDetailsLayout = ({ children }: StudentDetailsLayoutProps) => {
  return (
    <div className={styles['student-details-layout']}>
      <DashboardHeader src={logoPurple} />
      <main className={styles['student-details-layout__main']}>{children}</main>
    </div>
  )
}

export default StudentDetailsLayout
