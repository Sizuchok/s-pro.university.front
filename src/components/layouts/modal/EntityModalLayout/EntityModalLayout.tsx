import { ReactNode } from 'react'
import styles from './entity-modal-layout.module.scss'
import { Link } from 'react-router-dom'
import { ReactComponent as CloseModalIcon } from '../../../../assets/svg/exit_modal_x_sign.svg'

type EntityModalLayoutProps = {
  returnTo: string
  children: ReactNode
}

const EntityModalLayout = ({ returnTo, children }: EntityModalLayoutProps) => {
  return (
    <div className={styles.overlay}>
      <div className={styles.modal}>
        <Link to={returnTo} className={styles.close}>
          <CloseModalIcon />
        </Link>
        <div className={styles.form}>{children}</div>
      </div>
    </div>
  )
}

export default EntityModalLayout
