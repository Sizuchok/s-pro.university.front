import { ReactNode } from 'react'
import styles from './auth-primary-layout.module.scss'

type AuthPrimaryLayoutProps = {
  children: ReactNode
}

const AuthPrimaryLayout = ({ children }: AuthPrimaryLayoutProps) => {
  return <div className={styles['auth-primary-layout']}>{children}</div>
}

export default AuthPrimaryLayout
