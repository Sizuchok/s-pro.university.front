import React from 'react'
import EntityTableRow from '../EntityTableRow/EntityTableRow'
import { Lector } from '../../../../types/entities/Lector'
import styles from './lector-row.module.scss'

type LectorRowProps = {
  lector: Lector
  gridTemplateColumns: string
}

const LectorRow = ({ lector, gridTemplateColumns }: LectorRowProps) => {
  return (
    <EntityTableRow entityId={lector.id} gridTemplateColumns={gridTemplateColumns}>
      <div className={styles.row__name}>{lector.name}</div>
      <div className={styles.row__surname}>{lector.surname ? lector.surname : '---'}</div>
      <div className={styles.row__email}>{lector.email}</div>
      <div className={styles.row__password}>{lector.password}</div>
    </EntityTableRow>
  )
}

export default LectorRow
