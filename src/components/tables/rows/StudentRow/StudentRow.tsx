import EntityTableRow from '../EntityTableRow/EntityTableRow'
import styles from './student-row.module.scss'
import { Student } from '../../../../types/entities/Student'
import { API_SERVER_URL } from '../../../../constants/constants'

type StudentRowProps = {
  student: Student
  gridTemplateColumns: string
}

const StudentRow = ({ student, gridTemplateColumns }: StudentRowProps) => {
  return (
    <EntityTableRow entityId={student.id} gridTemplateColumns={gridTemplateColumns}>
      <div className={styles.row__image}>
        {student.imagePath ? (
          <img src={`${API_SERVER_URL}/public/${student.imagePath}`} alt="" />
        ) : (
          <span className={styles.image_placeholder}>{student.name[0].toUpperCase()}</span>
        )}
      </div>
      <div className={styles.row__name}>{student.name}</div>
      <div className={styles.row__surname}>{student.surname}</div>
      <div className={styles.row__email}>{student.email}</div>
      <div className={styles.row__password}>{student.age}</div>
      <div className={styles.row__courses}>
        {student.courses?.length ? student.courses?.map(course => course.name).join(', ') : '---'}
      </div>
      <div className={styles.row__group}>{student.groupName ? student.groupName : '---'}</div>
    </EntityTableRow>
  )
}

export default StudentRow
