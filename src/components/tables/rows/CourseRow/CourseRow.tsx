import EntityTableRow from '../EntityTableRow/EntityTableRow'
import styles from './course-row.module.scss'
import { Course } from '../../../../types/entities/Course'

type CourseRowProps = {
  course: Course
  gridTemplateColumns: string
}

const CourseRow = ({ course, gridTemplateColumns }: CourseRowProps) => {
  return (
    <EntityTableRow entityId={course.id} gridTemplateColumns={gridTemplateColumns}>
      <div className={styles.row__name}>{course.name}</div>
      <div className={styles.row__description}>{course.description}</div>
      <div className={styles.row__hours}>{course.hours}</div>
      <div className={styles.row__students}>{course.studentsCount}</div>
    </EntityTableRow>
  )
}

export default CourseRow
