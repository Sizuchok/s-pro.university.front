import EntityTableRow from '../EntityTableRow/EntityTableRow'
import { Group } from '../../../../types/entities/Group'

type GroupRowProps = {
  group: Group
  gridTemplateColumns: string
}

const GroupRow = ({ group, gridTemplateColumns }: GroupRowProps) => {
  return (
    <EntityTableRow entityId={group.id} gridTemplateColumns={gridTemplateColumns}>
      <div>{group.name}</div>
    </EntityTableRow>
  )
}

export default GroupRow
