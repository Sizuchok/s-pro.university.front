import { ReactNode } from 'react'
import styles from './entity-table-row.module.scss'
import { ReactComponent as EditIcon } from '../../../../assets/svg/entity_row_edit_icon.svg'
import { Link } from 'react-router-dom'

type EntityTableRowProps = {
  entityId: number
  children: ReactNode
  gridTemplateColumns: string
}

const EntityTableRow = ({ entityId, gridTemplateColumns, children }: EntityTableRowProps) => {
  return (
    <div className={styles.row} style={{ gridTemplateColumns }}>
      {children}
      <Link className={styles.row__link} to={`edit/${entityId}`}>
        <EditIcon className={styles.row__icon} />
      </Link>
    </div>
  )
}

export default EntityTableRow
