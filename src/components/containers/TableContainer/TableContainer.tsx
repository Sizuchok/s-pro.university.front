import { ReactNode } from 'react'
import styles from './table-container.module.scss'

type TableContainerProps = {
  columns: string[]
  gridTemplateColumns: string
  children: ReactNode
}

const TableContainer = ({ columns, gridTemplateColumns, children }: TableContainerProps) => {
  return (
    <div className={styles.table}>
      <div className={styles.table__header} style={{ gridTemplateColumns }}>
        {columns.map(col => (
          <div key={col}>{col}</div>
        ))}
      </div>
      <div className={styles.table__body}>{children}</div>
    </div>
  )
}

export default TableContainer
