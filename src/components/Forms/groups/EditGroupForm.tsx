import { useEffect } from 'react'
import Button from '../../common/Button/Button'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { Group, UpdateGroup } from '../../../types/entities/Group'
import { editGroupSchema } from '../../../constants/validations/group-validation-form'

type EditGroupFormProps = {
  group: Group | null
  isLoading: boolean
  onSubmit: (data: UpdateGroup) => void
}

const EditGroupForm = ({ group, onSubmit, isLoading }: EditGroupFormProps) => {
  const {
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdateGroup>({ resolver: zodResolver(editGroupSchema) })

  useEffect(() => {
    setValue('name', group?.name)
  }, [group, setValue])

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="Group's new name"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Button title="Update" isLoading={isLoading} />
    </form>
  )
}

export default EditGroupForm
