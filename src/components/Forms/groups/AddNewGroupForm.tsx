import Button from '../../common/Button/Button'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { CreateGroup } from '../../../types/entities/Group'
import { createGroupSchema } from '../../../constants/validations/group-validation-form'

type EditGroupFormProps = {
  isLoading: boolean
  onSubmit: (data: CreateGroup) => void
}

const AddNewGroupForm = ({ onSubmit, isLoading }: EditGroupFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<CreateGroup>({ resolver: zodResolver(createGroupSchema), mode: 'onChange' })

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="Group's name"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Button title="Create" disabled={!isValid} isLoading={isLoading} />
    </form>
  )
}

export default AddNewGroupForm
