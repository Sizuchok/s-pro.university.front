import React, { useEffect, useState } from 'react'
import Button from '../../common/Button/Button'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { Lector, UpdateLector } from '../../../types/entities/Lector'
import { editLectorSchema } from '../../../constants/validations/lector-validation.form'

type EditLectorFormProps = {
  lector: Lector | null
  isLoading: boolean
  onSubmit: (data: UpdateLector) => void
}

const EditLectorForm = ({ lector, onSubmit, isLoading }: EditLectorFormProps) => {
  const {
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdateLector>({ resolver: zodResolver(editLectorSchema) })

  useEffect(() => {
    setValue('name', lector?.name ?? '')
    setValue('surname', lector?.surname)
    setValue('email', lector?.email ?? '')
  }, [lector, setValue])

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="Lector's new name"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Input
        id="surname"
        label="Surname"
        type="text"
        placeholder="Lector's new surname"
        errors={errors.surname}
        register={register('surname')}
        disabled={isLoading}
      />
      <Input
        id="email"
        label="Email"
        type="email"
        placeholder="Lector's new email address"
        errors={errors.email}
        register={register('email')}
        disabled={isLoading}
      />

      <Input
        id="password"
        label="Password"
        type="text"
        placeholder="Lector's new password"
        register={register('password')}
        errors={errors.password}
        disabled={isLoading}
      />
      <Button title="Update" isLoading={isLoading} />
    </form>
  )
}

export default EditLectorForm
