import React, { useEffect, useState } from 'react'
import Button from '../../common/Button/Button'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { CreateLector } from '../../../types/entities/Lector'
import { createLectorSchema } from '../../../constants/validations/lector-validation.form'

type AddNewLectorFormProps = {
  isLoading: boolean
  onSubmit: (data: CreateLector) => void
}

const AddNewLectorForm = ({ onSubmit, isLoading }: AddNewLectorFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<CreateLector>({ resolver: zodResolver(createLectorSchema), mode: 'onChange' })

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="Lector's name"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Input
        id="surname"
        label="Surname"
        type="text"
        placeholder="Lector's surname"
        errors={errors.surname}
        register={register('surname')}
        disabled={isLoading}
      />
      <Input
        id="email"
        label="Email"
        type="email"
        placeholder="Lector's email address"
        errors={errors.email}
        register={register('email')}
        disabled={isLoading}
      />

      <Input
        id="password"
        label="Password"
        type="text"
        placeholder="Lector's password"
        register={register('password')}
        errors={errors.password}
        disabled={isLoading}
      />
      <Button title="Create" disabled={!isValid} isLoading={isLoading} />
    </form>
  )
}

export default AddNewLectorForm
