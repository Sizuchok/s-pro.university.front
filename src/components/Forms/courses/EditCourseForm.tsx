import { useEffect } from 'react'
import Button from '../../common/Button/Button'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { Course, UpdateCourse } from '../../../types/entities/Course'
import { editCourseSchema } from '../../../constants/validations/course-validation-form'

type EditCourseFormProps = {
  course: Course | null
  isLoading: boolean
  onSubmit: (data: UpdateCourse) => void
}

const EditCourseForm = ({ course, onSubmit, isLoading }: EditCourseFormProps) => {
  const {
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdateCourse>({ resolver: zodResolver(editCourseSchema) })

  useEffect(() => {
    setValue('name', course?.name)
    setValue('description', course?.description)
    setValue('hours', course?.hours)
  }, [course, setValue])

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="Course's new name"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Input
        id="description"
        label="Description"
        type="text"
        placeholder="Course's new description"
        errors={errors.description}
        register={register('description')}
        disabled={isLoading}
      />
      <Input
        id="hours"
        label="Hours"
        type="number"
        placeholder="Course's hours"
        errors={errors.hours}
        register={register('hours')}
        disabled={isLoading}
      />
      <Button title="Update" isLoading={isLoading} />
    </form>
  )
}

export default EditCourseForm
