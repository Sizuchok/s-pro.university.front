import Button from '../../common/Button/Button'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { CreateCourse } from '../../../types/entities/Course'
import { createCourseSchema } from '../../../constants/validations/course-validation-form'

type EditCourseFormProps = {
  isLoading: boolean
  onSubmit: (data: CreateCourse) => void
}

const AddNewCoureForm = ({ onSubmit, isLoading }: EditCourseFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<CreateCourse>({ resolver: zodResolver(createCourseSchema), mode: 'onChange' })

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="Course's name"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Input
        id="description"
        label="Description"
        type="text"
        placeholder="Course's description"
        errors={errors.description}
        register={register('description')}
        disabled={isLoading}
      />
      <Input
        id="hours"
        label="Hours"
        type="number"
        placeholder="Course's hours"
        errors={errors.hours}
        register={register('hours')}
        disabled={isLoading}
      />
      <Button title="Create" disabled={!isValid} isLoading={isLoading} />
    </form>
  )
}

export default AddNewCoureForm
