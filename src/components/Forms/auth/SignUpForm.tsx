import React, { useState } from 'react'
import Button from '../../common/Button/Button'
import Checkbox from '../../common/Checkbox/Checkbox'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import styles from './sign-up-form.module.scss'
import { UserSignUpFormData } from '../../../types/entities/User'
import { zodResolver } from '@hookform/resolvers/zod'
import { signUpSchema } from '../../../constants/validations/user-validation.forms'

type SignUpFormProps = {
  onSubmit: (data: UserSignUpFormData) => void
  isLoading: boolean
}

const SignUpForm = ({ onSubmit, isLoading }: SignUpFormProps) => {
  const [showPassword, setShowPassword] = useState(false)

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<UserSignUpFormData>({ resolver: zodResolver(signUpSchema), mode: 'onChange' })

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="What's your name?"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Input
        id="email"
        label="Email"
        type="email"
        placeholder="What's your email address?"
        errors={errors.email}
        register={register('email')}
        disabled={isLoading}
      />

      <Input
        id="password"
        label="Password"
        type={showPassword ? 'text' : 'password'}
        placeholder="Enter your password"
        register={register('password')}
        errors={errors.password}
        disabled={isLoading}
      />
      <Input
        id="confirmPassword"
        label="Confirm Password"
        type={showPassword ? 'text' : 'password'}
        placeholder="Repeat your password"
        register={register('confirmPassword')}
        errors={errors.confirmPassword}
        disabled={isLoading}
      />
      <Checkbox
        label="Show Password"
        checked={showPassword}
        onChange={() => setShowPassword(!showPassword)}
        disabled={isLoading}
      />
      <Button title="Register" disabled={!isValid} isLoading={isLoading} />
    </form>
  )
}

export default SignUpForm
