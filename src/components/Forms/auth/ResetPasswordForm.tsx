import { useState } from 'react'
import Button from '../../common/Button/Button'
import Checkbox from '../../common/Checkbox/Checkbox'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { ResetPasswordFormData } from '../../../types/entities/User'
import { zodResolver } from '@hookform/resolvers/zod'
import { resetPasswordSchema } from '../../../constants/validations/user-validation.forms'

type ResetPasswordFormProps = {
  onSubmit: (data: ResetPasswordFormData) => void
  isLoading: boolean
}

const ResetPasswordForm = ({ onSubmit, isLoading }: ResetPasswordFormProps) => {
  const [showPassword, setShowPassword] = useState(false)

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<ResetPasswordFormData>({
    resolver: zodResolver(resetPasswordSchema),
    mode: 'onChange',
  })

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))}>
      <Input
        id="password"
        label="New Password"
        type={showPassword ? 'text' : 'password'}
        placeholder="Enter new password"
        register={register('password')}
        errors={errors.password}
        disabled={isLoading}
      />
      <Input
        id="confirmPassword"
        label="Confirm Password"
        type={showPassword ? 'text' : 'password'}
        placeholder="Repeat new password"
        register={register('confirmPassword')}
        errors={errors.confirmPassword}
        disabled={isLoading}
      />
      <Checkbox
        label="Show Password"
        checked={showPassword}
        onChange={() => setShowPassword(!showPassword)}
        disabled={isLoading}
      />
      <Button isLoading={isLoading} disabled={!isValid} title="Reset" />
    </form>
  )
}

export default ResetPasswordForm
