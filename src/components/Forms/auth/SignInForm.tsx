import { useState } from 'react'
import Button from '../../common/Button/Button'
import Checkbox from '../../common/Checkbox/Checkbox'
import Input from '../../common/Input/Input'
import styles from './sign-in-form.module.scss'
import { useForm } from 'react-hook-form'
import { UserCredentials } from '../../../types/entities/User'
import { zodResolver } from '@hookform/resolvers/zod'
import { signInSchema } from '../../../constants/validations/user-validation.forms'

type SignInFormProps = {
  onSubmit: (data: UserCredentials) => void
  isLoading: boolean
}

const SignInForm = ({ onSubmit, isLoading }: SignInFormProps) => {
  const [showPassword, setShowPassword] = useState(false)

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<UserCredentials>({ resolver: zodResolver(signInSchema), mode: 'onChange' })

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="email"
        label="Email"
        type="email"
        placeholder="What's your email address?"
        errors={errors.email}
        register={register('email')}
        disabled={isLoading}
      />
      <Input
        id="password"
        label="Password"
        type={showPassword ? 'text' : 'password'}
        placeholder="Enter your password"
        register={register('password')}
        errors={errors.password}
        disabled={isLoading}
      />
      <Checkbox
        label="Show Password"
        checked={showPassword}
        disabled={isLoading}
        onChange={() => setShowPassword(!showPassword)}
      />

      <Button title="Login" disabled={!isValid} isLoading={isLoading} />
    </form>
  )
}

export default SignInForm
