import Input from '../../../common/Input/Input'
import { Link } from 'react-router-dom'
import Button from '../../../common/Button/Button'
import styles from './forgot-password-form.module.scss'
import { useForm } from 'react-hook-form'
import { ForgotPasswordFormData } from '../../../../types/entities/User'
import { zodResolver } from '@hookform/resolvers/zod'
import { forgotPasswordSchema } from '../../../../constants/validations/user-validation.forms'

type ForgotPasswordFormProps = {
  onSubmit: (data: ForgotPasswordFormData) => void
  isLoading: boolean
}

const ForgotPasswordForm = ({ onSubmit, isLoading }: ForgotPasswordFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<ForgotPasswordFormData>({
    resolver: zodResolver(forgotPasswordSchema),
    mode: 'onChange',
  })

  return (
    <form
      className={styles['forgot-passsword-form']}
      onSubmit={handleSubmit(data => onSubmit(data))}
      noValidate
    >
      <Input
        id="email"
        label="Email"
        type="email"
        placeholder="name@mail.com"
        register={register('email')}
        errors={errors.email}
        disabled={isLoading}
      />
      <div className={styles['forgot-passsword-form__buttons']}>
        <Button isLoading={isLoading} disabled={!isValid} title="Reset" />
        <Button link secondary>
          <Link to={'/sign-in'}>Cancel</Link>
        </Button>
      </div>
    </form>
  )
}

export default ForgotPasswordForm
