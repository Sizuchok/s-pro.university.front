import Button from '../../common/Button/Button'
import Input from '../../common/Input/Input'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { CreateStudent } from '../../../types/entities/Student'
import { createStudentSchema } from '../../../constants/validations/student-validation.form'

type AddNewStudentFormProps = {
  isLoading: boolean
  onSubmit: (data: CreateStudent) => void
}

const AddNewStudentForm = ({ onSubmit, isLoading }: AddNewStudentFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<CreateStudent>({ resolver: zodResolver(createStudentSchema), mode: 'onChange' })

  return (
    <form onSubmit={handleSubmit(data => onSubmit(data))} noValidate>
      <Input
        id="name"
        label="Name"
        type="text"
        placeholder="Student's name"
        errors={errors.name}
        register={register('name')}
        disabled={isLoading}
      />
      <Input
        id="surname"
        label="Surname"
        type="text"
        placeholder="Student's surname"
        errors={errors.surname}
        register={register('surname')}
        disabled={isLoading}
      />
      <Input
        id="email"
        label="Email"
        type="email"
        placeholder="Student's email address"
        errors={errors.email}
        register={register('email')}
        disabled={isLoading}
      />

      <Input
        id="age"
        label="Age"
        type="number"
        placeholder="Student's age"
        register={register('age')}
        errors={errors.age}
        disabled={isLoading}
      />
      <Button title="Create" disabled={!isValid} isLoading={isLoading} />
    </form>
  )
}

export default AddNewStudentForm
