import { Controller, useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { updateStudentSchema } from '../../../../constants/validations/student-validation.form'
import Input from '../../../common/Input/Input'
import Button from '../../../common/Button/Button'
import styles from './edit-student-form.module.scss'
import Dropzone from 'react-dropzone'
import { ReactComponent as PlusIcon } from '../../../../assets/svg/image_upload_area_plus_icon.svg'
import ReplaceImageButton from '../../../common/ReplaceImageButton/ReplaceImageButton'
import { Course } from '../../../../types/entities/Course'
import { Group } from '../../../../types/entities/Group'
import { Student, UpdateStudentForm } from '../../../../types/entities/Student'
import { useEffect } from 'react'
import { API_SERVER_URL } from '../../../../constants/constants'
import { DropdownIndicatorPrimaryIcon } from '../../../react-select/DropdownIndicatorPrimaryIcon'
import { reactSelectPrimaryStyles } from '../../../../constants/react-select-styles/react-select-primary-styles'
import TypedReactSelect from '../../../common/TypedReactSelect/TypedReactSelect'
import { NumberStringOption } from '../../../../types/react-select'

type EditStudentFormProps = {
  courses: Course[] | null
  groups: Group[] | null
  editStudent: Student | null
  isLoading: boolean
  onSubmit: (data: UpdateStudentForm) => void
}

const EditStudentForm = ({
  courses,
  groups,
  editStudent,
  onSubmit,
  isLoading,
}: EditStudentFormProps) => {
  const coursesOptions =
    courses?.map(course => ({
      value: course.id,
      label: course.name,
    })) ?? []
  const groupsOptions = groups?.map(group => ({ value: group.id, label: group.name })) ?? []
  let studentImage = editStudent?.imagePath

  const {
    register,
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<UpdateStudentForm>({ resolver: zodResolver(updateStudentSchema) })

  useEffect(() => {
    setValue('name', editStudent?.name)
    setValue('surname', editStudent?.surname)
    setValue('email', editStudent?.email)
    setValue('age', editStudent?.age)
    setValue('groupId', editStudent?.groupId ?? undefined)
    setValue('courseIds', editStudent?.courses.map(course => course.id))
  }, [editStudent, setValue])

  return (
    <form
      className={styles['edit-student-form']}
      onSubmit={handleSubmit(data => onSubmit(data))}
      noValidate
    >
      <div className={styles['personal-info']}>
        <h2 className={styles.heading}>Personal information</h2>
        <div className={styles['image-upload']}>
          <Controller
            name="file"
            control={control}
            render={({ field: { onChange, onBlur } }) => (
              <Dropzone
                maxFiles={1}
                maxSize={10_000_000}
                multiple={false}
                accept={{
                  'image/jpeg': ['.jpg', '.jpeg'],
                  'image/png': ['.png'],
                }}
                onDrop={async acceptedFiles => {
                  setValue('file', acceptedFiles[0])
                  studentImage = undefined
                }}
              >
                {({ getRootProps, getInputProps, open, acceptedFiles }) => (
                  <>
                    <div {...getRootProps({ className: styles['upload-area'] })}>
                      {!!acceptedFiles.length ? (
                        <img src={URL.createObjectURL(acceptedFiles[0])} alt="preview" />
                      ) : (
                        studentImage && (
                          <img
                            src={`${API_SERVER_URL}/public/${editStudent?.imagePath}`}
                            alt="preview"
                          />
                        )
                      )}
                      <div className={styles['upload-area__center']}>
                        <PlusIcon />
                      </div>
                      <input
                        {...getInputProps({
                          onChange,
                          onBlur,
                        })}
                        type="file"
                      />
                    </div>
                    <div className={styles['upload-details']}>
                      <ReplaceImageButton
                        title="Replace"
                        disabled={!acceptedFiles.length}
                        onClick={open}
                      />
                      <p className={styles['file-status']}>
                        {acceptedFiles[0]?.name ?? 'No file chosen'}
                      </p>
                      <p className={styles['file-requirements']}>
                        Must be a .jpg or .png file smaller than 10MB and at least 400px by 400px.
                      </p>
                    </div>
                  </>
                )}
              </Dropzone>
            )}
          />
        </div>
        <Input
          id="name"
          label="Name"
          type="text"
          placeholder="Student's name"
          errors={errors.name}
          register={register('name')}
          disabled={isLoading}
        />
        <Input
          id="surname"
          label="Surname"
          type="text"
          placeholder="Student's surname"
          errors={errors.surname}
          register={register('surname')}
          disabled={isLoading}
        />
        <Input
          id="email"
          label="Email"
          type="email"
          placeholder="Student's email address"
          errors={errors.email}
          register={register('email')}
          disabled={isLoading}
        />
        <Input
          id="age"
          label="Age"
          type="number"
          placeholder="Student's age"
          register={register('age')}
          errors={errors.age}
          disabled={isLoading}
        />
        <Button title="Save changes" isLoading={isLoading} />
      </div>
      <div className={styles['courses-and-groups']}>
        <h2 className={styles.heading}>Courses and Groups</h2>

        <div>
          <label htmlFor="groupId" className={styles['select-label']}>
            Courses
          </label>
          <Controller
            name="courseIds"
            control={control}
            render={({ field: { onChange, value } }) => (
              <TypedReactSelect<NumberStringOption | undefined, true>
                components={{ DropdownIndicator: DropdownIndicatorPrimaryIcon }}
                isMulti
                isSearchable={false}
                options={coursesOptions}
                className={styles['select-primary']}
                value={value?.map(val => coursesOptions.find(({ value }) => value === val))}
                onChange={options => onChange(options.map(opt => opt?.value))}
                styles={reactSelectPrimaryStyles}
              />
            )}
          />
        </div>
        <div>
          <label htmlFor="groupId" className={styles['select-label']}>
            Group
          </label>
          <Controller
            name="groupId"
            control={control}
            render={({ field: { onChange, value } }) => (
              <TypedReactSelect<NumberStringOption, false>
                id="groupId"
                components={{ DropdownIndicator: DropdownIndicatorPrimaryIcon }}
                isSearchable={false}
                options={groupsOptions}
                className={styles['select-primary']}
                value={groupsOptions?.find(option => option.value === value)}
                onChange={option => onChange(option?.value)}
                styles={reactSelectPrimaryStyles}
              />
            )}
          />
        </div>
      </div>
    </form>
  )
}

export default EditStudentForm
