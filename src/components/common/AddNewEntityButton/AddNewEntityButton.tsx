import styles from './add-new-entity-button.module.scss'
import { ReactComponent as PlusSign } from '../../../assets/svg/add_new_enitty_plus_icon.svg'
import { Link } from 'react-router-dom'

type AddNewEntityButtonProps = {
  to: string
  title: string
}

const AddNewEntityButton = ({ to, title }: AddNewEntityButtonProps) => {
  return (
    <Link to={to} className={styles['add-new-entity-button']} type="button">
      <PlusSign />
      {title}
    </Link>
  )
}

export default AddNewEntityButton
