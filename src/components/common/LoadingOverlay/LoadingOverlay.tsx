import { CircularProgress } from '@mui/material'
import styles from './loading-overlay.module.scss'

const LoadingOverlay = () => {
  return (
    <div className={styles['loading-overlay']}>
      <CircularProgress color="inherit" size={'4rem'} />
    </div>
  )
}

export default LoadingOverlay
