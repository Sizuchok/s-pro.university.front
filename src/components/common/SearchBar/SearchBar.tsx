import styles from './search-bar.module.scss'
import { ReactComponent as SearchIcon } from '../../../assets/svg/search_icon.svg'
import { ChangeEvent, useState } from 'react'

type SearchBarProps = {
  query: string
  onChange: (event: ChangeEvent<HTMLInputElement>) => void
}

const SearchBar = ({ query, onChange }: SearchBarProps) => {
  return (
    <div className={styles['search-bar']}>
      <SearchIcon className={styles['search-bar__icon']} />
      <input
        className={styles['search-bar__input']}
        type="text"
        placeholder="Search"
        value={query}
        onChange={onChange}
      />
    </div>
  )
}

export default SearchBar
