import classNames from 'classnames'
import styles from './replace-image-button.module.scss'

type ReplaceImageButtonProps = {
  title: string
  disabled?: boolean
  onClick: () => void
}

const ReplaceImageButton = ({ title, disabled, onClick }: ReplaceImageButtonProps) => {
  const buttonClassNames = classNames({
    [styles['replace-image-button']]: true,
    [styles['disabled']]: disabled,
  })
  return (
    <button type="button" className={buttonClassNames} disabled={disabled} onClick={onClick}>
      {title}
    </button>
  )
}

export default ReplaceImageButton
