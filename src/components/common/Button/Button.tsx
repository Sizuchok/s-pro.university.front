import React from 'react'
import styles from './button.module.scss'
import classNames from 'classnames'
import { CircularProgress } from '@mui/material'

type ButtonProps = {
  title?: string
  link?: boolean
  secondary?: boolean
  disabled?: boolean
  isLoading?: boolean
  children?: React.ReactNode
}

const Button = ({ title, link, secondary, disabled, isLoading, children }: ButtonProps) => {
  const btnClassNames = classNames(styles.button, {
    [styles['button--secondary']]: secondary,
    [styles['button--primary']]: !secondary,
    [styles['button-link']]: link,
  })

  return link ? (
    <div className={btnClassNames}>{children}</div>
  ) : (
    <button disabled={isLoading || disabled} type="submit" className={btnClassNames}>
      {isLoading ? <CircularProgress color="inherit" size="1.5rem" /> : title}
    </button>
  )
}

export default Button
