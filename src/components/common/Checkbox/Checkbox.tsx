import styles from './checkbox.module.scss'

type ButtonProps = {
  label: string
  checked: boolean
  disabled?: boolean
  onChange: () => void
}

const Checkbox = ({ label, checked, disabled, onChange }: ButtonProps) => {
  return (
    <div className={styles.checkbox}>
      <input
        className={styles.checkbox__input}
        type="checkbox"
        checked={checked}
        onChange={onChange}
        disabled={disabled}
      />
      <label className={styles.checkbox__label}>{label}</label>
    </div>
  )
}

export default Checkbox
