import React from 'react'
import ReactPaginate from 'react-paginate'
import styles from './pagination.module.scss'

type PaginationProps = {
  currentPage: number
  pageCount: number
  onPageChange: (page: number) => void
}

const Pagination = ({ currentPage, pageCount, onPageChange }: PaginationProps) => {
  const handlePageClick = (data: { selected: number }) => {
    onPageChange(data.selected)
  }

  return (
    <ReactPaginate
      previousLabel={'\u2190'}
      nextLabel={'\u2192'}
      breakLabel={'...'}
      breakClassName={styles['break-me']}
      pageCount={pageCount}
      marginPagesDisplayed={2}
      pageRangeDisplayed={5}
      onPageChange={handlePageClick}
      containerClassName={styles.pagination}
      activeClassName={styles.active}
      forcePage={currentPage}
    />
  )
}

export default Pagination
