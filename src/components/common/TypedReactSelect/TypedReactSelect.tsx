import Select, { GroupBase, Props } from 'react-select'

const TypedReactSelect = <
  Option,
  IsMulti extends boolean = false,
  Group extends GroupBase<Option> = GroupBase<Option>,
>(
  props: Props<Option, IsMulti, Group>,
) => {
  return <Select {...props} />
}

export default TypedReactSelect
