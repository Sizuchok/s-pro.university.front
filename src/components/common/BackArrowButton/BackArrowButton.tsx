import { Link } from 'react-router-dom'
import styles from './back-arrow-button.module.scss'
import { ReactComponent as BackArrowIcon } from '../../../assets/svg/back_arrow_icon.svg'

type BackArrowButtonProps = {
  to: string
}

const BackArrowButton = ({ to }: BackArrowButtonProps) => {
  return (
    <Link className={styles['back-arrow']} to={to}>
      <BackArrowIcon />
      Back
    </Link>
  )
}

export default BackArrowButton
