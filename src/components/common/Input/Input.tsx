import { FieldError, FieldErrorsImpl, Merge, UseFormRegisterReturn } from 'react-hook-form'
import styles from './input.module.scss'
import classNames from 'classnames'

type InputProps = {
  id: string
  label: string
  type: 'text' | 'email' | 'password' | 'number'
  placeholder: string
  disabled?: boolean
  register?: UseFormRegisterReturn<string>
  errors?: FieldError | Merge<FieldError, FieldErrorsImpl<any>> | undefined
}

const Input = ({ id, label, type, placeholder, disabled, register, errors }: InputProps) => {
  const labelClassNames = classNames({
    [styles.input__label]: true,
    [styles.error]: errors,
  })

  const inputFieldClassNames = classNames({
    [styles.input__field]: true,
    [styles.error]: errors,
  })

  return (
    <>
      <div className={styles.input}>
        <label className={labelClassNames} htmlFor={id}>
          {label}
        </label>
        <input
          {...register}
          className={inputFieldClassNames}
          id={id}
          type={type}
          placeholder={placeholder}
          disabled={disabled}
        />
        {errors && <p className={styles['input__error-messages']}>{errors.message?.toString()}</p>}
      </div>
    </>
  )
}

export default Input
