import styles from './form-title.module.scss'
import logo from '../../../assets/svg/logo.svg'
import React from 'react'
import classNames from 'classnames'

type FormTitleProps = {
  title: string
  noLogo?: boolean
  message?: string
  messageAlignLeft?: boolean
}

const FormTitle = ({ title, noLogo, message, messageAlignLeft }: FormTitleProps) => {
  const messageClassNames = classNames({
    [styles.title__message]: true,
    [styles['title__message--align-left']]: messageAlignLeft,
  })

  return (
    <div className={styles.title}>
      {noLogo ?? <img className={styles.title__logo} src={logo} alt="A logo" />}
      <h3 className={styles.title__heading}>{title}</h3>
      {message && <p className={messageClassNames}>{message}</p>}
    </div>
  )
}

export default FormTitle
