import styles from './dashboard-sidebar.module.scss'
import logo from '../../../assets/svg/logo_dashboard.svg'
import DashboardTab from '../DashboardTab/DashboardTab'
import dashboardTabIcon from '../../../assets/dashboard_tabs_icons/dashboard.svg'
import coursesTabIcon from '../../../assets/dashboard_tabs_icons/courses.svg'
import lectorsTabIcon from '../../../assets/dashboard_tabs_icons/lectors.svg'
import groupsTabIcon from '../../../assets/dashboard_tabs_icons/groups.svg'
import studentsTabIcon from '../../../assets/dashboard_tabs_icons/students.svg'
import logoutTabIcon from '../../../assets/dashboard_tabs_icons/logout.svg'
import { useAppDispatch } from '../../../utils/hooks'
import { logOut } from '../../../redux/auth/auth.creators'
import { reset } from '../../../redux/auth/authSlice'
import { useLocation } from 'react-router-dom'

const DashboardSidebar = () => {
  const dispatch = useAppDispatch()
  const pathName = useLocation().pathname

  const handleLogOut = () => {
    dispatch(logOut())
    dispatch(reset())
  }

  return (
    <aside className={styles.sidebar}>
      <header className={styles.sidebar__header}>
        <img src={logo} alt="" />
      </header>
      <div className={styles.sidebar__tabs}>
        <div className={styles['sidebar__tabs__dashboard']}>
          <DashboardTab
            to="/dashboard"
            name="Dashboard"
            icon={dashboardTabIcon}
            active={pathName.includes('/dashboard')}
          />
        </div>

        <DashboardTab
          to="/courses"
          name="Courses"
          icon={coursesTabIcon}
          active={pathName.includes('/courses')}
        />
        <DashboardTab
          to="/lectors"
          name="Lectors"
          icon={lectorsTabIcon}
          active={pathName.includes('/lectors')}
        />
        <DashboardTab
          to="/groups"
          name="Groups"
          icon={groupsTabIcon}
          active={pathName.includes('/groups')}
        />
        <DashboardTab
          to="/students"
          name="Students"
          icon={studentsTabIcon}
          active={pathName.includes('/students')}
        />

        <DashboardTab to="/sign-in" onClick={handleLogOut} name="Log out" icon={logoutTabIcon} />
      </div>
    </aside>
  )
}

export default DashboardSidebar
