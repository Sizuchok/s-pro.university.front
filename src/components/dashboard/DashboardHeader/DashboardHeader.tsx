import styles from './dashboard-header.module.scss'
import lectorImage from '../../../assets/avatars/lector_photo.png'
import classNames from 'classnames'

type DashboardHeaderProps = {
  tabName?: string
  src?: string
}

const DashboardHeader = ({ src, tabName }: DashboardHeaderProps) => {
  const headerClassNames = classNames({
    [styles['dashboard-header']]: true,
    [styles['no-border']]: src,
  })

  return (
    <header className={headerClassNames}>
      {src ? (
        <img className={styles['dashboard-header__tab-heading']} src={src} alt="" />
      ) : (
        <h1 className={styles['dashboard-header__tab-heading']}>{tabName}</h1>
      )}
      <div className={styles['dashboard-header__image']}>
        <img src={lectorImage} alt="" />
      </div>
    </header>
  )
}

export default DashboardHeader
