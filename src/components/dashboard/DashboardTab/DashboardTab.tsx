import { Link } from 'react-router-dom'
import styles from './dashboard-tab.module.scss'
import classNames from 'classnames'

type DashboardTabProps = {
  name: string
  icon: string
  to: string
  active?: boolean
  onClick?: () => void
}

const DashboardTab = ({ name, icon, to, active, onClick }: DashboardTabProps) => {
  const tabClassName = classNames({
    [styles.tab]: true,
    [styles.active]: active,
  })

  return (
    <Link to={to} onClick={onClick} className={tabClassName}>
      <img className={styles.tab__icon} src={icon} alt="" />
      <span className={styles.tab__name}>{name}</span>
    </Link>
  )
}

export default DashboardTab
