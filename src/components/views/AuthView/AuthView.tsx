import { ReactNode } from 'react'
import styles from './auth-view.module.scss'

type AuthViewProps = {
  children: ReactNode
}

const AuthView = ({ children }: AuthViewProps) => {
  return <div className={styles['auth-view']}>{children}</div>
}

export default AuthView
