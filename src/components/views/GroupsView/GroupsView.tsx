import AddNewEntityButton from '../../common/AddNewEntityButton/AddNewEntityButton'
import SearchBar from '../../common/SearchBar/SearchBar'
import styles from './groups-view.module.scss'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { ChangeEvent, useEffect, useState } from 'react'
import TableContainer from '../../containers/TableContainer/TableContainer'
import { useDebounce } from 'use-debounce'
import { parseSelectValueToQuery } from '../../../utils/parse-select-value-to-query'
import { Controller, useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { BaseQueryParams } from '../../../types/QueryParams'
import Pagination from '../../common/Pagination/Pagination'
import { reactSelectDashboardStyles } from '../../../constants/react-select-styles/react-select-dashboard-styles'
import { getAllGroups } from '../../../redux/groups/groups.creators'
import { resetGroups } from '../../../redux/groups/groups.slice'
import GroupRow from '../../tables/rows/GroupRow/GroupRow'
import { baseSortOptions } from '../../../constants/entity-view-base-sort-options'
import LoadingOverlay from '../../common/LoadingOverlay/LoadingOverlay'
import { useSearchParams } from 'react-router-dom'
import TypedReactSelect from '../../common/TypedReactSelect/TypedReactSelect'
import { StringStringOption } from '../../../types/react-select'

const sortOptions = [...baseSortOptions]

const gridTemplateColumns = '1fr 24px'
const columns = ['Name']

const GroupsView = () => {
  const [searchParams, setSearchParams] = useSearchParams()

  const dispatch = useAppDispatch()
  const { isLoading, isSuccess, isError, message, list, count } = useAppSelector(
    state => state.groups,
  )

  const [query, setQuery] = useState(searchParams.get('query') ?? '')
  const [debouncedQuery] = useDebounce(query, 500)

  const [currentPage, setCurrentPage] = useState(0)
  const itemsPerPage = 10

  const handleQueryChange = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    searchParams.set('query', value)
    setSearchParams(searchParams)
    setQuery(value)
  }

  const { control, watch } = useForm()

  const selectedOption = watch('selectedOption')

  const getSelectOptionFromQueryParam = () => {
    const sortBy = searchParams.get('sortBy')
    const order = searchParams.get('order')

    return sortOptions.find(option => option.value === `${sortBy}-${order}`) ?? sortOptions[0]
  }

  useEffect(() => {
    const { sortBy, order } = parseSelectValueToQuery(
      selectedOption?.value ?? getSelectOptionFromQueryParam().value,
    )
    const queryParams: BaseQueryParams = {}

    if (sortBy && order) {
      queryParams.sortBy = sortBy
      queryParams.order = order
    }

    queryParams.query = debouncedQuery
    queryParams.limit = itemsPerPage.toString()

    const offset = currentPage * itemsPerPage
    if (offset) queryParams.offset = offset.toString()

    dispatch(getAllGroups(queryParams))
  }, [dispatch, debouncedQuery, selectedOption, currentPage])

  useEffect(() => {
    if (isError) toast.error(message)

    if (isError || isSuccess) dispatch(resetGroups())
  }, [isError, isSuccess, message, dispatch])

  return (
    <>
      <div className={styles['groups-view__filters']}>
        <label className={styles['sort-by-label']}>Sort by</label>
        <Controller
          name="selectedOption"
          control={control}
          render={({ field: { onChange } }) => (
            <TypedReactSelect<StringStringOption, false>
              isSearchable={false}
              options={sortOptions}
              styles={reactSelectDashboardStyles}
              className={styles.sort}
              defaultValue={sortOptions[0]}
              onChange={option => {
                const { order, sortBy } = parseSelectValueToQuery(option?.value)
                searchParams.set('order', order)
                searchParams.set('sortBy', sortBy)
                setSearchParams(searchParams)
                onChange(option)
              }}
              value={getSelectOptionFromQueryParam()}
            />
          )}
        />
        <SearchBar query={query} onChange={handleQueryChange} />
        <AddNewEntityButton to={`add`} title="Add new group" />
      </div>

      {isLoading ? (
        <LoadingOverlay />
      ) : (
        <TableContainer columns={columns} gridTemplateColumns={gridTemplateColumns}>
          {(list ?? []).map(group => (
            <GroupRow
              group={group}
              key={group.id + group.name}
              gridTemplateColumns={gridTemplateColumns}
            />
          ))}
        </TableContainer>
      )}

      <Pagination
        currentPage={currentPage}
        pageCount={Math.ceil(count / itemsPerPage) || 1}
        onPageChange={page => setCurrentPage(page)}
      />
    </>
  )
}

export default GroupsView
