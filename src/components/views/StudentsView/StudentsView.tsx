import AddNewEntityButton from '../../common/AddNewEntityButton/AddNewEntityButton'
import SearchBar from '../../common/SearchBar/SearchBar'
import Select from 'react-select'
import styles from './students-view.module.scss'
import { useAppDispatch, useAppSelector } from '../../../utils/hooks'
import { ChangeEvent, useEffect, useState } from 'react'
import TableContainer from '../../containers/TableContainer/TableContainer'
import { useDebounce } from 'use-debounce'
import { parseSelectValueToQuery } from '../../../utils/parse-select-value-to-query'
import { Controller, useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { BaseQueryParams } from '../../../types/QueryParams'
import Pagination from '../../common/Pagination/Pagination'
import { reactSelectDashboardStyles } from '../../../constants/react-select-styles/react-select-dashboard-styles'
import { baseSortOptions } from '../../../constants/entity-view-base-sort-options'
import { getAllStudents } from '../../../redux/students/students.creators'
import { resetStudents } from '../../../redux/students/students.slice'
import StudentRow from '../../tables/rows/StudentRow/StudentRow'
import { useSearchParams } from 'react-router-dom'
import LoadingOverlay from '../../common/LoadingOverlay/LoadingOverlay'
import TypedReactSelect from '../../common/TypedReactSelect/TypedReactSelect'
import { StringStringOption } from '../../../types/react-select'

const sortOptions = [...baseSortOptions]

const gridTemplateColumns = '88px 0.16fr 0.2fr 0.25fr 0.1fr 0.25fr 0.1fr 24px'
const columns = ['', 'Name', 'Surname', 'Email', 'Age', 'Courses', 'Group']

const StudentsView = () => {
  const [searchParams, setSearchParams] = useSearchParams()

  const dispatch = useAppDispatch()
  const { isLoading, isSuccess, isError, message, list, count } = useAppSelector(
    state => state.students,
  )

  const [query, setQuery] = useState(searchParams.get('query') ?? '')
  const [debouncedQuery] = useDebounce(query, 500)

  const [currentPage, setCurrentPage] = useState(0)
  const itemsPerPage = 10

  const handleQueryChange = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    searchParams.set('query', value)
    setSearchParams(searchParams)
    setQuery(value)
  }

  const { control, watch } = useForm()

  const selectedOption = watch('selectedOption')

  const getSelectOptionFromQueryParam = () => {
    const sortBy = searchParams.get('sortBy')
    const order = searchParams.get('order')

    return sortOptions.find(({ value }) => value === `${sortBy}-${order}`) ?? sortOptions[0]
  }

  useEffect(() => {
    const { sortBy, order } = parseSelectValueToQuery(
      selectedOption?.value ?? getSelectOptionFromQueryParam().value,
    )
    const queryParams: BaseQueryParams = {}

    if (sortBy && order) {
      queryParams.sortBy = sortBy
      queryParams.order = order
    }

    queryParams.query = debouncedQuery
    queryParams.limit = itemsPerPage.toString()

    const offset = currentPage * itemsPerPage
    if (offset) queryParams.offset = offset.toString()

    dispatch(getAllStudents(queryParams))
  }, [dispatch, debouncedQuery, selectedOption, currentPage])

  useEffect(() => {
    if (isError) toast.error(message)

    if (isError || isSuccess) dispatch(resetStudents())
  }, [isError, isSuccess, message, dispatch])

  return (
    <>
      <div className={styles['students-view__filters']}>
        <label className={styles['sort-by-label']}>Sort by</label>
        <Controller
          name="selectedOption"
          control={control}
          render={({ field: { onChange } }) => (
            <TypedReactSelect<StringStringOption, false>
              isSearchable={false}
              options={sortOptions}
              styles={reactSelectDashboardStyles}
              className={styles.sort}
              defaultValue={sortOptions[0]}
              onChange={option => {
                const { order, sortBy } = parseSelectValueToQuery(option?.value)
                searchParams.set('order', order)
                searchParams.set('sortBy', sortBy)
                setSearchParams(searchParams)
                onChange(option)
              }}
              value={getSelectOptionFromQueryParam()}
            />
          )}
        />
        <SearchBar query={query} onChange={handleQueryChange} />
        <AddNewEntityButton to={`add`} title="Add new student" />
      </div>

      {isLoading ? (
        <LoadingOverlay />
      ) : (
        <TableContainer columns={columns} gridTemplateColumns={gridTemplateColumns}>
          {(list ?? []).map(student => (
            <StudentRow
              gridTemplateColumns={gridTemplateColumns}
              student={student}
              key={student.id + student.name}
            />
          ))}
        </TableContainer>
      )}

      <Pagination
        currentPage={currentPage}
        pageCount={Math.ceil(count / itemsPerPage) || 1}
        onPageChange={page => setCurrentPage(page)}
      />
    </>
  )
}

export default StudentsView
