import { DropdownIndicatorProps, components } from 'react-select'

export const DropdownIndicatorPrimaryIcon = (props: DropdownIndicatorProps<any, any>) => {
  return (
    <components.DropdownIndicator {...props}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="black"
      >
        <path d="M12 14.5L17 9.5H7L12 14.5Z" fill="black" />
      </svg>
    </components.DropdownIndicator>
  )
}
