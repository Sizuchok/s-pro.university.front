import React from 'react'
import { Outlet } from 'react-router-dom'
import DashboardPrimaryLayout from '../components/layouts/dashboard/DashboardPrimaryLayout'
import CoursesView from '../components/views/CoursesView/CoursesView'

const Courses = () => {
  return (
    <DashboardPrimaryLayout tabName="Courses">
      <CoursesView />
      <Outlet />
    </DashboardPrimaryLayout>
  )
}

export default Courses
