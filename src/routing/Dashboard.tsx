import DashboardPrimaryLayout from '../components/layouts/dashboard/DashboardPrimaryLayout'
import { Outlet } from 'react-router-dom'

const Dashboard = () => {
  return (
    <DashboardPrimaryLayout tabName="Dashboard">
      <Outlet />
    </DashboardPrimaryLayout>
  )
}

export default Dashboard
