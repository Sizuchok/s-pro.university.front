import React from 'react'
import { Outlet } from 'react-router-dom'
import DashboardPrimaryLayout from '../components/layouts/dashboard/DashboardPrimaryLayout'
import GroupsView from '../components/views/GroupsView/GroupsView'

const Groups = () => {
  return (
    <DashboardPrimaryLayout tabName="Groups">
      <GroupsView />
      <Outlet />
    </DashboardPrimaryLayout>
  )
}

export default Groups
