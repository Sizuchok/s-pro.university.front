import AuthView from '../components/views/AuthView/AuthView'
import AuthPrimaryLayout from '../components/layouts/auth/AuthPrimaryLayout'
import SignInForm from '../components/Forms/auth/SignInForm'
import ResetPasswordLink from '../components/auth/ResetPasswordLink/ResetPasswordLink'
import FormTitle from '../components/common/FormTitle/FormTitle'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { signIn } from '../redux/auth/auth.creators'
import { UserCredentials } from '../types/entities/User'
import { useAppDispatch, useAppSelector } from '../utils/hooks'
import { reset } from '../redux/auth/authSlice'
import { toast } from 'react-toastify'

const SignIn = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isLoading, isSuccess, isError, message } = useAppSelector(state => state.auth)

  const onSubmit = (data: UserCredentials) => {
    dispatch(signIn(data))
  }

  useEffect(() => {
    if (isSuccess) navigate('/lectors')

    if (isError) toast.error(message)

    if (isError || isSuccess) dispatch(reset())
  }, [dispatch, navigate, isSuccess, isError, message])

  return (
    <AuthView>
      <AuthPrimaryLayout>
        <FormTitle title="Welcome!" />
        <SignInForm isLoading={isLoading} onSubmit={onSubmit} />
        <ResetPasswordLink />
      </AuthPrimaryLayout>
    </AuthView>
  )
}

export default SignIn
