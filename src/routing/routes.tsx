import { Navigate, createBrowserRouter } from 'react-router-dom'
import SignUp from './SignUp'
import ForgotPassword from './ForgotPassword'
import ResetPassword from './ResetPassword'
import Dashboard from './Dashboard'
import SignIn from './SignIn'
import PrivateRoutes from './PrivateRoutes'
import PublicRoutes from './PublicRoutes'
import Lectors from './Lectors'
import EditLector from '../components/modals/lectors/EditLector'
import AddNewLector from '../components/modals/lectors/AddNewLector'
import Students from './Students'
import Courses from './Courses'
import EditCourse from '../components/modals/courses/EditCourse'
import AddNewCourse from '../components/modals/courses/AddNewCourse'
import Groups from './Groups'
import EditGroup from '../components/modals/groups/EditGroup'
import AddNewGroup from '../components/modals/groups/AddNewGroup'
import EditStudent from './EditStudent'
import AddNewStudent from '../components/modals/students/AddNewStudent'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Navigate to={'/sign-in'} />,
  },
  {
    element: <PublicRoutes />,
    children: [
      {
        path: '/sign-in',
        element: <SignIn />,
      },
      {
        path: '/sign-up',
        element: <SignUp />,
      },
      {
        path: '/forgot-password',
        element: <ForgotPassword />,
      },
    ],
  },
  {
    path: '/reset-password',
    element: <ResetPassword />,
  },
  {
    element: <PrivateRoutes />,
    children: [
      {
        path: '/dashboard',
        element: <Dashboard />,
        children: [],
      },
      // LECTORS
      {
        path: '/lectors',
        element: <Lectors />,
        children: [
          {
            path: 'edit/:id',
            element: <EditLector />,
          },
          {
            path: 'add',
            element: <AddNewLector />,
          },
        ],
      },
      // COURSES
      {
        path: '/courses',
        element: <Courses />,
        children: [
          {
            path: 'edit/:id',
            element: <EditCourse />,
          },
          {
            path: 'add',
            element: <AddNewCourse />,
          },
        ],
      },
      // GROUPS
      {
        path: '/groups',
        element: <Groups />,
        children: [
          {
            path: 'edit/:id',
            element: <EditGroup />,
          },
          {
            path: 'add',
            element: <AddNewGroup />,
          },
        ],
      },
      // STUDENTS
      {
        path: '/students',
        element: <Students />,
        children: [
          {
            path: 'add',
            element: <AddNewStudent />,
          },
          {
            path: 'edit/:id',
            element: <EditStudent />,
          },
        ],
      },
    ],
  },
])

export default router
