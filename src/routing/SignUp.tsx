import FormTitle from '../components/common/FormTitle/FormTitle'
import AuthView from '../components/views/AuthView/AuthView'
import AuthPrimaryLayout from '../components/layouts/auth/AuthPrimaryLayout'
import SignUpForm from '../components/Forms/auth/SignUpForm'
import SignInLink from '../components/auth/SignInLink/SignInLink'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { signUp } from '../redux/auth/auth.creators'
import { UserSignUpFormData } from '../types/entities/User'
import { useAppDispatch, useAppSelector } from '../utils/hooks'
import { reset } from '../redux/auth/authSlice'
import { toast } from 'react-toastify'

const SignUp = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()

  const { isLoading, isSuccess, isError, message } = useAppSelector(state => state.auth)

  const onSubmit = (data: UserSignUpFormData) => {
    const { confirmPassword, ...dataToSend } = data
    dispatch(signUp(dataToSend))
  }

  useEffect(() => {
    if (isSuccess) navigate('/lectors')

    if (isError) toast.error(message)

    if (isError || isSuccess) dispatch(reset())
  }, [dispatch, navigate, isSuccess, isError, message])

  return (
    <AuthView>
      <AuthPrimaryLayout>
        <FormTitle title="Register your account" />
        <SignUpForm isLoading={isLoading} onSubmit={onSubmit} />
        <SignInLink />
      </AuthPrimaryLayout>
    </AuthView>
  )
}

export default SignUp
