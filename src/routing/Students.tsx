import { Outlet } from 'react-router-dom'
import DashboardPrimaryLayout from '../components/layouts/dashboard/DashboardPrimaryLayout'
import StudentsView from '../components/views/StudentsView/StudentsView'

const Students = () => {
  return (
    <DashboardPrimaryLayout tabName="Students">
      <StudentsView />
      <Outlet />
    </DashboardPrimaryLayout>
  )
}

export default Students
