import { Navigate, Outlet } from 'react-router-dom'
import { ACCESS_TOKEN_KEY } from '../constants/constants'

const PublicRoutes = () => {
  const token = localStorage.getItem(ACCESS_TOKEN_KEY)

  return !token ? <Outlet /> : <Navigate to="/lectors" replace />
}

export default PublicRoutes
