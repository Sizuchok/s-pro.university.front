import { useNavigate, useParams } from 'react-router-dom'
import BackArrowButton from '../components/common/BackArrowButton/BackArrowButton'
import StudentDetailsLayout from '../components/layouts/students/StudentDetailsLayout'
import {
  addStudentImage,
  getStudentById,
  updateStudentById,
} from '../redux/students/students.creators'
import { UpdateStudentForm } from '../types/entities/Student'
import { useAppDispatch, useAppSelector } from '../utils/hooks'
import { useEffect } from 'react'
import { getAllCourses } from '../redux/courses/courses.creators'
import { getAllGroups } from '../redux/groups/groups.creators'
import EditStudentForm from '../components/Forms/students/EditStudentForm/EditStudentForm'
import { resetStudents } from '../redux/students/students.slice'

const EditStudent = () => {
  const returnTo = '/students'
  const navigate = useNavigate()
  const dispatch = useAppDispatch()

  const params = useParams()
  const id = Number(params.id)
  if (!id) navigate(returnTo)

  const { isLoading, isSuccess, editStudent } = useAppSelector(state => state.students)
  const { list: courses } = useAppSelector(state => state.courses)
  const { list: groups } = useAppSelector(state => state.groups)

  const onSubmit = async (data: UpdateStudentForm) => {
    const { file, ...studentData } = data

    await dispatch(updateStudentById({ id, data: studentData }))

    if (file) dispatch(addStudentImage({ id, data: { file } }))
  }

  useEffect(() => {
    dispatch(getStudentById(id))
    dispatch(getAllCourses({}))
    dispatch(getAllGroups({}))
  }, [])

  useEffect(() => {
    if (isSuccess) navigate(returnTo)
  }, [dispatch, navigate, isSuccess])

  return (
    <StudentDetailsLayout>
      <BackArrowButton to={returnTo} />
      <EditStudentForm
        editStudent={editStudent}
        groups={groups}
        courses={courses}
        isLoading={isLoading}
        onSubmit={onSubmit}
      />
    </StudentDetailsLayout>
  )
}

export default EditStudent
