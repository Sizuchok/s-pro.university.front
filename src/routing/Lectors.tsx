import { Outlet } from 'react-router-dom'
import DashboardPrimaryLayout from '../components/layouts/dashboard/DashboardPrimaryLayout'
import LectorsView from '../components/views/LectorsView/LectorsView'

const Lectors = () => {
  return (
    <DashboardPrimaryLayout tabName="Lectors">
      <LectorsView />
      <Outlet />
    </DashboardPrimaryLayout>
  )
}

export default Lectors
