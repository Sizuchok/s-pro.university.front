import { useEffect } from 'react'
import ResetPasswordForm from '../components/Forms/auth/ResetPasswordForm'
import AuthView from '../components/views/AuthView/AuthView'
import AuthPrimaryLayout from '../components/layouts/auth/AuthPrimaryLayout'
import FormTitle from '../components/common/FormTitle/FormTitle'
import { Link, Navigate, useLocation } from 'react-router-dom'
import Button from '../components/common/Button/Button'
import { RESET_PASSWORD_URL_ID_KEY, RESET_PASSWORD_URL_TOKEN_KEY } from '../constants/constants'
import { ResetPasswordData, ResetPasswordFormData } from '../types/entities/User'
import { resetPassword } from '../redux/auth/auth.creators'
import { useAppDispatch, useAppSelector } from '../utils/hooks'
import { toast } from 'react-toastify'
import { reset } from '../redux/auth/authSlice'

const ResetPassword = () => {
  const dispatch = useAppDispatch()
  const location = useLocation()

  const { isLoading, isSuccess, isError, message } = useAppSelector(state => state.auth)

  const urlQueryParams = new URLSearchParams(location.search)
  const resetToken = urlQueryParams.get(RESET_PASSWORD_URL_TOKEN_KEY)
  const userId = urlQueryParams.get(RESET_PASSWORD_URL_ID_KEY)

  const onSubmit = (data: ResetPasswordFormData) => {
    const resetPasswordDataToSend: ResetPasswordData = {
      lectorId: +userId!,
      token: resetToken!,
      newPassword: data.password,
    }
    dispatch(resetPassword(resetPasswordDataToSend))
  }

  useEffect(() => {
    if (isError) {
      toast.error(message)
      dispatch(reset())
    }
  }, [dispatch, isError, message])

  if (!resetToken && !userId) return <Navigate to="/sign-in" replace />

  return (
    <AuthView>
      <AuthPrimaryLayout>
        {!isSuccess ? (
          <>
            <FormTitle title="Reset Your Password" />
            <ResetPasswordForm onSubmit={onSubmit} isLoading={isLoading} />
          </>
        ) : (
          <>
            <FormTitle
              title="Password changed"
              message="You can use your new password to log into your account."
              messageAlignLeft
            />
            <Button link>
              <Link to="/sign-in">Logi In</Link>
            </Button>
          </>
        )}
      </AuthPrimaryLayout>
    </AuthView>
  )
}

export default ResetPassword
