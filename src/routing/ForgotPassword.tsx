import FormTitle from '../components/common/FormTitle/FormTitle'
import AuthView from '../components/views/AuthView/AuthView'
import AuthPrimaryLayout from '../components/layouts/auth/AuthPrimaryLayout'
import { useEffect } from 'react'
import { toast } from 'react-toastify'
import { forgotPassword } from '../redux/auth/auth.creators'
import { reset } from '../redux/auth/authSlice'
import { ForgotPasswordFormData } from '../types/entities/User'
import { useAppDispatch, useAppSelector } from '../utils/hooks'
import ForgotPasswordForm from '../components/Forms/auth/ForgotPasswordForm/ForgotPasswordForm'

const ForgotPassword = () => {
  const dispatch = useAppDispatch()

  const { isLoading, isSuccess, isError, message } = useAppSelector(state => state.auth)

  const onSubmit = (data: ForgotPasswordFormData) => {
    dispatch(forgotPassword(data))
  }

  useEffect(() => {
    if (isError) {
      toast.error(message)
      dispatch(reset())
    }
  }, [dispatch, isError, message])

  return (
    <AuthView>
      <AuthPrimaryLayout>
        {isSuccess ? (
          <FormTitle
            title="Check your email"
            message="We've sent a password reset link to the email address you provided. You can now close this tab."
          />
        ) : (
          <>
            <FormTitle
              title="Reset Password"
              message="Don't worry, happens to the best of us. Enter the email address associated with your
          account and we'll send you a link to reset."
              messageAlignLeft
            />
            <ForgotPasswordForm onSubmit={onSubmit} isLoading={isLoading} />
          </>
        )}
      </AuthPrimaryLayout>
    </AuthView>
  )
}

export default ForgotPassword
