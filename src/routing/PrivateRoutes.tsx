import { useEffect } from 'react'
import { Navigate, Outlet, useNavigate } from 'react-router-dom'
import { ACCESS_TOKEN_KEY } from '../constants/constants'
import { useAppDispatch, useAppSelector } from '../utils/hooks'
import { getMe } from '../redux/auth/auth.creators'
import { reset } from '../redux/auth/authSlice'

const PrivateRoutes = () => {
  console.log('render')
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { isError, user } = useAppSelector(state => state.auth)
  console.log(user)

  useEffect(() => {
    if (isError) {
      dispatch(reset())
      navigate('/sign-in')
    }

    dispatch(getMe())
  }, [dispatch, navigate, isError])

  const token = localStorage.getItem(ACCESS_TOKEN_KEY)

  return token ? <Outlet /> : <Navigate to="/sign-in" replace />
}

export default PrivateRoutes
